/**
 * Created by komrakov on 02.03.15.
 */
(function () {

    angular
        .module('yanews')
        .directive('spinner', spinner);

    function spinner() {
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div></div>'
        }
    }

})();