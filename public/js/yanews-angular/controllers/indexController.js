/**
 * Created by komrakov on 12.03.15.
 */
(function () {

    angular
        .module('yanews')
        .controller('indexController', indexController);

    function indexController($rootScope, $scope, itemsService, regionsService, $location, $filter, cyService) {
        moment.locale('ru');

        /* for datetimepicker */
        var timestamp = Date.now();
        $scope.maxDate = new Date(timestamp);
        $scope.initDate = new Date(timestamp - (7*24*60*60*1000));
        $scope.items_time = {
            date_from: $scope.initDate,
            date_to: $scope.maxDate
        };

        $scope.items_region_time = {
            date_from: $scope.initDate,
            date_to: $scope.maxDate
        };

        /* for daterangepicker */
        var month_ranges = ['Текущий месяц', 'Предыдущий месяц'];

        var getActiveRange = function() {
            var elem = $('.daterangepicker .ranges .active');
            if (elem.length > 0) {
                return elem.text();
            } else {
                return 'Текущий месяц';
            }
        };

        var min = new Date('01/01/2015');
        $scope.datepicker = {
            date: {
                startDate: moment().startOf('month'),
                endDate: moment().subtract('month').endOf('month')
            },
            min: min,
            options: {
                ranges: {
                    'Текущий месяц':  [moment().startOf('month'), moment().subtract('month').endOf('month')],
                    'Предыдущий месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'За все время' : [min, moment()]
                },
                //showDropdowns: true,
                autoApply: true,
                locale: {
                    format: 'DD.MM.YYYY',
                    applyLabel: 'Применить',
                    cancelLabel: 'Отмена',
                    customRangeLabel: 'Другое'
                },
                eventHandlers: {
                    'apply.daterangepicker': function(ev, picker) {
                        initSourcesTime();
                    }
                }
            }
        };

        $rootScope.$on('LoadingItems', function (event, status) { $scope.LoadingItems = status; });
        $scope.region_id = 0;
        $scope.cy_region_id = 0;
        $scope.pageSize = 100;
        $scope.page = 0;

        itemsService.items.query.limit = itemsService.items_region.query.limit  = $scope.pageSize;

        $scope.tab_active = {items:true, sources:false, items_region:false, cy:false};

        for (var tab in $scope.tab_active) {
            $scope.tab_active[tab] = $location.path() == '/' + tab;
        }

        $scope.changeTab = function(tab) {
            $location.path(tab);
        };

        $scope.selectItems = function () {
            $scope.changeTab('items');
            $scope.items = itemsService.getItems();
        };

        $scope.selectSources = function () {
            initSourcesTime();
            $scope.changeTab('sources');
        };

        $scope.getSources = function (region_id) {
            itemsService.sources.query.region_id = region_id;
            $scope.sources = itemsService.getSources();
        };

        $scope.changePage = function (page) {
            $scope.items.query.skip = (page-1) * $scope.pageSize;
            itemsService.getItems();
        };

        $scope.dateOptions = {
            startingDay: 1,
            showWeeks: false
        };

        $scope.selectTime = function(region) {
            if (!region) {
                $scope.items.query.skip = 0;
                initItemsTime();
                itemsService.getItems();
            } else {
                $scope.items_region = itemsService.items_region;
                $scope.items_region.query.region = region;
                $scope.items_region.query.skip = 0;
                initItemsRegionTime();
                itemsService.getItemsRegion();
            }
        };

        $scope.downloadItems = function() {
            initItemsTime();
            itemsService.downloadItems();
        };

        $scope.downloadSources = function(region_id) {
            itemsService.sources.query.region_id = region_id;
            itemsService.downloadSources();
        };

        $scope.regions = regionsService.getRegions();

        $scope.cy_regions = regionsService.getCyRegions();

        var sort_fields = ['new', 'total'];

        $scope.setSortField = function(field) {

            if ($filter('inArray')(field, sort_fields)) {
                itemsService.sources.query.sort_order =
                    (itemsService.sources.query.sort_order === null || itemsService.sources.query.sort_field != field)
                        ? 1
                        : -itemsService.sources.query.sort_order;
                itemsService.sources.query.sort_field = field;
            } else {
                itemsService.sources.query.sort_order = 1;
                itemsService.sources.query.sort_field = null;
            }
        };

        var initItemsTime = function() {
            $scope.items.query.date_from = Date.parse($scope.items_time.date_from)/1000;
            $scope.items.query.date_to = Date.parse($scope.items_time.date_to)/1000;
        };

        var initItemsRegionTime = function() {
            $scope.items_region.query.date_from = Date.parse($scope.items_region_time.date_from)/1000;
            $scope.items_region.query.date_to = Date.parse($scope.items_region_time.date_to)/1000;
        };

        var initSourcesTime = function() {
            itemsService.sources.query.date_from = $scope.datepicker.date.startDate.unix();
            itemsService.sources.query.date_to   = $scope.datepicker.date.endDate.unix();
        };

        $scope.downloadPdf = function(region_id) {

            if ($filter('inArray')(getActiveRange(), month_ranges)) {
                itemsService.sources.query.format = 'month';
            } else {
                itemsService.sources.query.format = 'period';
            }

            itemsService.sources.query.region_id = region_id;

            itemsService.downloadPdf();
        };

        $scope.checkCy = function(domain) {
            $scope.cy = null;
            $rootScope.$broadcast('LoadingItems', true);
            cyService.checkCy(domain).then(function(response) {
                if (response.status == 'Ok') {
                    $scope.cy = response.data;
                    $scope.error = '';
                } else {
                    $scope.error = response.data;
                    $scope.cy = null;
                }
                $rootScope.$broadcast('LoadingItems', false);

            }, function(response) {
                $scope.error = response.data.data;
                $scope.cy = null;
                $rootScope.$broadcast('LoadingItems', false);
            });

        }

    }

})();