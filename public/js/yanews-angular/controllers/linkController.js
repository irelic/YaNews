/**
 * Created by nikolaev on 01.03.16.
 */
(function () {

    angular
        .module('yanews')
        .controller('linkController', linkController);

    function linkController($rootScope, $scope, linkService) {
        $rootScope.$on('LoadingHosts', function (event, status) { $scope.LoadingHosts = status; });
        $scope.hosts = [];
        $scope.checkLink = function(link) {
            $rootScope.$broadcast('LoadingHosts', true);
            $scope.media = linkService.checkLink(link).then(function(response) {
                if (response.status == 'Ok') {
                    $scope.hosts = response.data;
                    $scope.error = '';
                } else {
                    $scope.error = response.data;
                    $scope.hosts = [];
                }
                $rootScope.$broadcast('LoadingHosts', false);

            });
        }
    }

})();