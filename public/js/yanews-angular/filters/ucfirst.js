/**
 * Created by komrakov on 02.03.15.
 */
(function () {

    angular
        .module('yanews')
        .filter('ucfirst', ucfirst);

    function ucfirst() {
        return function(input) {
            if(typeof input !== 'string') {
                return input;
            }
            return input.split(' ').map(function (ch) {
                return ch.charAt(0).toUpperCase() + ch.substring(1);
            }).join(' ');
        }
    }

})();