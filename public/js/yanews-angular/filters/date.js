/**
 * Created by komrakov on 12.03.15.
 */
(function () {

    angular
        .module('yanews')
        .filter('toMskTimezone', toMskTimezone)
        .filter('msk_date', msk_date);

    function toMskTimezone() {
        return function (time) {
            var date = new Date(time);
            var MSKoffcet = 3 * 60;
            var offset = date.getTimezoneOffset();
            var tzDifference = (MSKoffcet + offset) * 60 * 1000;
            time = time + tzDifference;

            return time;
        }
    }

    function msk_date($filter) {
        return function (time, string) {
            time = $filter('toMskTimezone')(time);
            return $filter('date')(time, string);
        }
    }

})();