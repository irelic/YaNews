/**
 * Created by nikolaev on 21.03.16.
 */


(function () {

    angular
        .module('yanews')
        .filter('sum', sum);
    function sum() {
        return function (data, field) {
            var total = 0;
            for(var i = 0; i < data.length; i++){
                total += parseInt(data[i][field]);
            }
            return total;
        };
    }
})();
