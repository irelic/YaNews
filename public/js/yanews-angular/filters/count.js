/**
 * Created by komrakov on 12.03.15.
 */
(function () {

    angular
        .module('yanews')
        .filter('count', count);

    function count() {
        return function(input) {
            var counter = 0;
            if (typeof input == "object") {
                for (var element in input) {
                    if (input.hasOwnProperty(element))
                        counter++;
                }
            }
            else if (typeof input == "array") counter = input.length;

            return counter;
        }
    }

})();