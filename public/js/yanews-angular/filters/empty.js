/**
 * Created by komrakov on 02.03.15.
 */
(function () {

    angular
        .module('yanews')
        .filter('empty', empty);

    function empty() {
        return function(mixed_var) {
            return ( (typeof mixed_var == 'undefined') || mixed_var === "" || mixed_var === 0   || mixed_var === "0" || mixed_var === null  || mixed_var === false  ||  ( ( mixed_var instanceof Array ) && mixed_var.length === 0 ) );
        }
    }

})();