/**
 * Created by nikolaev on 21.09.15.
 */
(function () {

    angular
        .module('yanews')
        .filter('inArray', inArray);

    function inArray() {
        return function (needle, haystack, strict) {
            var found = false, key;
            strict = !!strict;
            for (key in haystack) {
                if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
                    found = true;
                    break;
                }
            }
            return found;
        };
    }

})();