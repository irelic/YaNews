/**
 * Created by komrakov on 02.03.15.
 */
(function () {

    angular
        .module('yanews', ['ngResource', 'ngSanitize', 'ui.bootstrap', 'ngDialog', 'ui.bootstrap.datetimepicker', 'tableSort', 'daterangepicker'])
        .config(Config);

    function Config($interpolateProvider, $locationProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false,
            rewriteLinks: false
        });
    }

})();