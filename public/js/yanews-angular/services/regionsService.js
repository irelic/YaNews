/**
 * Created by shark on 3/2/15.
 */
(function () {

    angular
        .module('yanews')
        .service('regionsService', regionsService);

    function regionsService($rootScope, $resource) {
        var self = this;
        self.regions = {
            query: {},
            data: [{'text': 'Выберите регион', _id: 0}],
            count: 0
        };
        self.cy_regions = {
            query: {},
            data: [{'text': 'Выберите регион', _id: 0}],
            count: 0
        };
        self.Api = $resource("/api/regions/:id", {id: "@id"}, {
            cy: { method:'GET', url: "/api/regions/cy" }
        });
        self.getRegions = function() {
            self.Api.get(self.regions.query, function(response, headers) {
                self.regions.data = self.regions.data.concat(response.data);
            });
            return self.regions;
        };

        self.getCyRegions = function() {
            self.Api.cy(self.cy_regions.query, function(response, headers) {
                self.cy_regions.data = self.cy_regions.data.concat(response.data);
            });
            return self.cy_regions;
        };
    }

})();