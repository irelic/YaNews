/**
 * Created by shark on 3/2/15.
 */
(function () {

    angular
        .module('yanews')
        .service('itemsService', itemsService);

    function itemsService($rootScope, $resource) {
        var feedsService = this;
        feedsService.items = {
            query: { limit: 0, skip: 0, date_from: null, date_to: null, type: 'items', format: null },
            data: [],
            count: 0
        };
        feedsService.items_region = {
            query: { limit: 0, skip: 0, date_from: null, date_to: null, type: 'items_region', format: null, region: null },
            data: [],
            count: 0
        };
        feedsService.sources = {
            query: { region_id: 0, date_from: null, date_to: null, sort_field:null, sort_order:null, type: 'sources' },
            data: []
        };
        feedsService.Api = $resource("/api/items/:id", {id: "@id"}, {
            download: { method:'GET', url: "/api/items/download" },
            pdf: { method:'GET', url: "/api/items/pdf" },
            sources: { method:'GET', url: "/api/items/sources" }
        });
        feedsService.getItems = function() {
            feedsService.items.data = [];
            $rootScope.$broadcast('LoadingItems', true);
            feedsService.Api.get(feedsService.items.query, function(response, headers) {
                feedsService.items.count = parseInt(headers("count"));
                feedsService.items.data  = response.data;
                $rootScope.$broadcast('LoadingItems', false);
            });
            return feedsService.items;
        };

        feedsService.getItemsRegion = function() {
            feedsService.items_region.data = [];
            $rootScope.$broadcast('LoadingItems', true);
            feedsService.Api.get(feedsService.items_region.query, function(response, headers) {
                feedsService.items_region.count = parseInt(headers("count"));
                feedsService.items_region.data  = response.data;
                $rootScope.$broadcast('LoadingItems', false);
            });
            return feedsService.items_region;
        };

        feedsService.getSources = function() {
            feedsService.sources.data = [];
            $rootScope.$broadcast('LoadingItems', true);
            feedsService.Api.sources(feedsService.sources.query, function(response) {
                feedsService.sources.data = response.data;
                $rootScope.$broadcast('LoadingItems', false);
            });
            return feedsService.sources;
        };
        feedsService.downloadItems = function () {
            feedsService.Api.download(feedsService.items.query, function(data) {
                window.open(data.data, '_blank');
            });
        };
        feedsService.downloadSources = function () {
            feedsService.Api.download(feedsService.sources.query, function(data) {
                window.open(data.data, '_blank');
            });
        };
        feedsService.downloadPdf = function () {
            $rootScope.$broadcast('LoadingItems', true);
            feedsService.Api.pdf(feedsService.sources.query, function(data) {
                $rootScope.$broadcast('LoadingItems', false);
                window.open(data.data, '_blank');
            });
        };
    }

})();