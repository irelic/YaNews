/**
 * Created by nikolaev on 01.03.16.
 */
(function () {

    angular
        .module('yanews')
        .service('cyService', cyService);

    function cyService($rootScope, $resource) {
        var self = this;
        self.Api = $resource("/api/cy", {}, {});

        self.checkCy = function(domain){
            return self.Api.get({domain: domain}).$promise;
        }
    }
})();
