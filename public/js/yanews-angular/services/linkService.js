/**
 * Created by nikolaev on 01.03.16.
 */
(function () {

    angular
        .module('yanews')
        .service('linkService', linkService);

    function linkService($rootScope, $resource) {
        var self = this;
        self.Api = $resource("/api/link", {}, {
            check: { method:'POST', url: "/api/link/check", link: '@link' },
        });

        self.checkLink = function(link){
            return self.Api.check({link: link}).$promise;
        }
    }
})();
