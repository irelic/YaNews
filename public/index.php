<?php

error_reporting(E_ALL);

try {

    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Access-Token");

    if (!defined('ROOT_PATH')) {
        define('ROOT_PATH', dirname(__DIR__));
    }

    require( ROOT_PATH . '/app/modules/ErrorHandler.php');
    ErrorHandler::register();

    require( ROOT_PATH . '/vendor/autoload.php');

    /**
     * Read the configuration
     */
    $config = include ROOT_PATH . "/app/config/config.php";

    /**
     * Read services
     */
    include ROOT_PATH . "/app/config/loader.php";
    include ROOT_PATH . "/app/config/services.php";
    loadWebDI($di, $loader);

    CDI()->clientResolver->setClient('yanews');

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);

    $uri = '/';
    if (!empty($_SERVER['REQUEST_URI'])) {
        $uriParts = explode('?', $_SERVER['REQUEST_URI']);
        $uri = $uriParts[0];
    }

    echo $application->handle($uri)->getContent();

} catch (\Exception $e) {
    ErrorHandler::exception($e);
}

