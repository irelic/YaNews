/**
 * Created by nikolaev on 18.09.15.
 */
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    minifyCss = require('gulp-minify-css');

var paths = {
    css: [
        'public/css/**/*.css'
    ],
    scripts: [
        'public/js/jquery-2.1.3/jquery-2.1.3.min.js',
        'public/js/bootstrap/bootstrap.js',
        'public/js/angular-1.3.14/base/*.js',
        'public/js/angular-1.3.14/modules/*.js',
        'public/js/momentjs/*.js',
        'public/js/bootstrap-daterangepicker/*.js',
        'public/js/angular-daterangepicker/*.js',
        'public/js/yanews-angular/yanews.js',
        'public/js/yanews-angular/controllers/*.js',
        'public/js/yanews-angular/services/*.js',
        'public/js/yanews-angular/directives/*.js',
        'public/js/yanews-angular/filters/*.js',
        'public/js/yanews-angular/modules/*.js'
    ],
    fonts: [
        'public/fonts/**/*.*'
    ],
    clean: 'build'
};

gulp.task('css', function() {
    gulp.src(paths.css)
        .pipe(concat('css.min.css'))
        .pipe(minifyCss())
        .pipe(gulp.dest('public/build/css'));
});

gulp.task('scripts', function () {
    return gulp.src(paths.scripts)
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest('public/build/js'));
});

gulp.task('fonts', function() {
    gulp.src(paths.fonts)
        .pipe(gulp.dest('public/build/fonts'))
});

gulp.task('build', [
    'css',
    'scripts',
    'fonts'
]);

gulp.task('watch', function(){
    gulp.watch(paths.css, function(event, cb) {
        gulp.start('css');
    });
    gulp.watch(paths.scripts, function(event, cb) {
        gulp.start('scripts');
    });
});


gulp.task('default', [ 'build' ]);
