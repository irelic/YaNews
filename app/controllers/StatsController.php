<?php
/**
 * Controller for statistics functions
 *
 * @author :  komrakov
 * @date   :  20.08.14 9:56
 */

namespace App\Controllers;

use models\Categories;
use \Phalcon\Http\Request;
use Pheanstalk\Pheanstalk;
use queue\QueueStats;

class StatsController extends ControllerBase{

    public function beforeExecuteRoute(){
        // Для переменной page берем только первую часть пути урла без get-параметров
        $this->view->setVar('page', explode('?', explode('/', trim($this->request->getURI(), '/'))[0])[0]);
        parent::beforeExecuteRoute();
    }

    public function beanstalkAction($tube = "default")
    {
        $pheanstalk = new Pheanstalk(CDI()->config->beanstalk->host);

        $tubes = $pheanstalk->listTubes();
        $this->view->setVar('tubes', $tubes );
        $this->view->setVar('stats', $pheanstalk->stats() );
        $this->view->setVar('stats_tube', $pheanstalk->statsTube($tube) );
        $this->view->setVar('active_tube', $tube );
    }

    /**
     * Clear passed tube (or all at once)
     * @param $tube
     */
    public function clearTubeAction($tube)
    {
        /*
         * WARNING: Only for developer!!!
         */
        return false;
        if ($tube == 'default'){
            foreach(QueueStats::getTubes() as $name){
                if ($name !== 'default'){
                    QueueStats::clearTube($name);
                }
            }
        } else {
            QueueStats::clearTube($tube);
        }
        $this->response->redirect('beanstalk');
    }

} 
