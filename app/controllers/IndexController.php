<?php

namespace App\Controllers;

use Phalcon\Mvc\View;

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->hide_navigation = true;
    }

    public function maintenanceAction()
    {
        $this->view->hide_navigation = true;
    }

    public function oldBrowserAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function backendAction()
    {
        $this->view->disable();
    }


}

