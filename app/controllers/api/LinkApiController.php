<?php namespace App\Controllers\Api;
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 01.03.16
 * Time: 12:18
 */
use yandex\Ratings;
use yandex\Satellites;

/**
 * @RoutePrefix("/api/link")
 */
class LinkApiController extends ApiControllerBase
{

    /**
     * @Post('/check')
     */
    public function checkAction()
    {
        $request = $this->request->getJsonRawBody();

        if (empty($request->link)) {
            return $this->showError('Отсутствует ссылка');
        }

        $host = parse_url($request->link, PHP_URL_HOST);
        $path = parse_url($request->link, PHP_URL_PATH);
        if (!strstr($host, 'news.yandex.ru') || !isset($path)) {
            return $this->showError('Неверный адрес ссылки');
        }

        $hosts = Satellites::processHosts($request->link);
        $result = [];
        foreach ($hosts as $i => $host) {
            $result[$i]['host'] = $host;
            $result[$i]['rating'] = Ratings::getRating($host);
        }

        return $this->showSuccess($result);

    }

}