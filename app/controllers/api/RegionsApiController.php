<?php namespace App\Controllers\Api;
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 19.11.15
 * Time: 10:49
 */
use models\Regions;
use yandex\YandexRegions;

/**
 * @RoutePrefix("/api/regions")
 */
class RegionsApiController extends ApiControllerBase
{
    /**
     * @Get("/")
     */
    public function getRegionsAction()
    {
        return $this->showSuccess(Regions::find());
    }

    /**
     * @Get("/cy")
     */
    public function getCyRegionsAction()
    {
        $cy_regions = CDI()->config->cy_regions->toArray();

        $regions = (Regions::find([['string_id' => ['$in' => $cy_regions]]]));

        return $this->showSuccess($regions);
    }


}