<?php
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.03.15
 * Time: 18:13
 */
namespace App\Controllers\Api;
use models\Items;
use models\Sources;
use pdf\PDFAggregator;
use Phalcon\Http\Request;
use Phalcon\Mvc\View;
use yandex\Ratings;

/**
 * @RoutePrefix("/api/items")
 */
class ItemsApiController extends ApiControllerBase
{
    /**
     * @Get("/")
     */
    public function getItemsAction()
    {
        $items = new Items();
        $limit = $this->request->get('limit');
        $skip  = $this->request->get('skip');
        $from  = $this->request->get('date_from');
        $to    = $this->request->get('date_to');
        $region = $this->request->get('region');
        $this->showSuccess($items->getItems($from, $to, $limit, $skip, $region));
        $this->response->setHeader("count", $items->getItemsCount($from, $to, $region));
    }

    /**
     * @Get("/sources")
     */
    public function getSourcesAction()
    {
        $items      = new Sources();
        $region_id  = $this->request->get('region_id');
        $from       = $this->request->get('date_from');
        $to         = $this->request->get('date_to');
        $sort_field = $this->request->get('sort_field');
        $sort_order = $this->request->get('sort_order');
        $items = $items->getSources($region_id, $from, $to, $sort_field, $sort_order);
        foreach ($items as $i => $item) {
            $items[$i]['rating'] = Ratings::getRating($item['domain']);
        }

        $this->showSuccess($items);
    }

    /**
     * @Get("/download")
     */
    public function downloadAction()
    {
        $type       = $this->request->get('type');
        $from       = $this->request->get('date_from');
        $to         = $this->request->get('date_to');
        $region_id  = $this->request->get('region_id');
        $sort_field = $this->request->get('sort_field');
        $sort_order = $this->request->get('sort_order');

        if ($type == 'items') {
            $data = (new Items())->aggregateToCsv($from, $to);
        } elseif ($type = 'sources') {
            $data = (new Sources())->aggregateToCsv($from, $to, $region_id, $sort_field, $sort_order);
        } else {
            return $this->showError('wrong type!');
        }

        $filename = CDI()->config->files_path . 'content_'.$type.'.xls';
        file_put_contents($filename, $data);
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachement; filename="'.$filename.'";');
        $this->showSuccess('/temp/content_'.$type.'.xls');
    }

    /**
     * @Get("/pdf")
     */
    public function pdfAction()
    {
        $type = $this->request->get('format');
        $from = $this->request->get('date_from');
        $to   = $this->request->get('date_to');
        $region_id  = $this->request->get('region_id');
        $sort_field = $this->request->get('sort_field');

        $filename = PDFAggregator::sources($type, $region_id, $from, $to, $sort_field);

        return $this->showSuccess('/temp/' . $filename);
    }
}