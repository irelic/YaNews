<?php namespace App\Controllers\Api;
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 01.03.16
 * Time: 12:18
 */
use yandex\Ratings;
use yandex\Satellites;

/**
 * @RoutePrefix("/api/cy")
 */
class CyApiController extends ApiControllerBase
{

    /**
     * @Get('/')
     */
    public function getAction()
    {
        $domain = $this->request->get('domain');

        if (empty($domain)) {
            return $this->showError('Отсутствует домен');
        }

        try {
            $cy = Ratings::getRating($domain, true);
        } catch (\Exception $e) {
            return $this->showError('Не удалось получить ТИЦ', 422);
        }

        return $this->showSuccess($cy);
    }

}