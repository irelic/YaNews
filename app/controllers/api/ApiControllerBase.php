<?php

namespace App\Controllers\Api;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Request;

class ApiControllerBase extends Controller
{
    public function beforeExecuteRoute()
    {
        $this->view->disable();

        return true;
    }

    /**
     * @param string|array $data
     * @return bool
     */
    public function showSuccess($data = "Success")
    {
        if ($this->response->isSent()) {
            return false;
        }

        $this->response->setJsonContent([
            "status" => "Ok",
            "data"   => $data
        ]);

        $this->response->send();
    }

    /**
     * @param string|array $data
     * @param int $statusCode
     * @return bool
     */
    public function showError($data, $statusCode = 200)
    {
        if ($this->response->isSent()) {
            return false;
        }

        $this->response->setJsonContent([
            "status" => "Error",
            "data"   => $data
        ]);

        $this->response->setStatusCode($statusCode);
        $this->response->send();
    }


}
