<?php

namespace App\Controllers;

use Phalcon\Mvc\Controller,
    Phalcon\DI;

class ControllerBase extends Controller
{

    public function beforeExecuteRoute()
    {
        $this->view->setVar('config', $this->config);

        $this->view->setVar('controller', $this->dispatcher->getControllerName());
    }

    /**
     * @param string|array $data
     */
    public function showSuccess($data)
    {
        $this->response->setJsonContent([
            "status" => "Ok",
            "data"   => $data
        ]);
        $this->response->send();
    }

    /**
     * @param string|array $data
     */
    public function showError($data)
    {
        $this->response->setJsonContent([
            "status" => "Error",
            "data"   => $data
        ]);
        $this->response->send();
    }

    public function route404Action()
    {
        $this->view->hide_navigation = true;
    }

}
