<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs([
    $config->application->componentsDir,
    $config->application->tasks,
]);

$loader->registerNamespaces([
    'App\Controllers'     => $config->application->controllers,
    'App\Controllers\Api' => $config->application->api,
    'models'              => $config->application->models,
    'App\Exceptions'      => $config->application->exceptions

]);

$loader->register();