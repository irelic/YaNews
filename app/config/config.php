<?php
/**
 * Main Config
 *
 * @author :  ignat
 * @date   :  16.03.15 13:48
 */

return new \Phalcon\Config([

    // Used in twig views. Example {{ config.app_name }} outputs "Экстремизм.NET"
    'app_name' => "Yandex News Analyzer",

    // Allow [development, production, maintenance]
    'stage' => 'development',
    'stage_detection' => true,

    // SuperAdmin role
    'admin_role' => 'developer',

    'mongo' => [
        'default' => [
            'nodes'        => [],
            'host'         => 'localhost',
            'replicaName'  => '',
            'port'         => 27017,
            'database'     => 'yandex_catalog',
        ],

        'production' => [
            'nodes' => [
//                ['host' => 'db1.viewer.ru','port' => 27017],
//                ['host' => 'db2.viewer.ru','port' => 27017]
            ],
            'host'        => 'localhost',
//            'replicaName' => 'rsv0',
            'port'         => 27017,
            'database'     => 'yandex_catalog',
        ]
    ],

    'beanstalk' => [
        'host'              => '127.0.0.1',
        'port'              => '11300',
        'jobReserveTimeout' => 0,
        'listenerDelay'     => 5,
        'reconnectTimeout'  => 10,
        'maxReleases'       => 3,
        'connectPersistent' => true,
        // Время резерва задачи 5 минут. После истечения времени, задача возвращаетя в очередь как необработанная
        'default_ttr'       => 300,
    ],

    'redis' => [
        'host' => '127.0.0.1',
        'port' => 6379
    ],

    'application' => [
        'baseUri'       => '/',
        'app'           => ROOT_PATH . '/app/',
        'componentsDir' => ROOT_PATH . '/app/modules/',
        'controllers'   => ROOT_PATH . '/app/controllers/',
        'api'           => ROOT_PATH . '/app/controllers/api/',
        'models'        => ROOT_PATH . '/app/models/',
        'views'         => ROOT_PATH . '/app/views/',
        'cacheDir'      => ROOT_PATH . '/app/cache/',
        'tasks'         => ROOT_PATH . '/app/tasks',
        'exceptions'    => ROOT_PATH . '/app/exceptions/',
    ],

    'log' => [
        'store_logs_time' => 2592000, // 60*60*24*30 (30 days)
        'userlogs_time_offset' => 0,  // +7 hours
        'dev_active'  => true,
        'user_active' => true,
        'dev'      => ROOT_PATH . '/logs/dev.log',
        'gv_entry' => ROOT_PATH . '/logs/gv_entry.log',
        'query'    => ROOT_PATH . '/logs/query.log',
        'profile'  => ROOT_PATH . '/logs/profile.log',
    ],

    /**
     * Для начала надо оставить ограничение по времени. Хотя бы для тестов.
     * Ограничение по единовременному сбору сообщений тоже надо оставить
     */
    'grabbers' => [
        'message_limit'    => 1000,
        'message_lifetime' => 86400
    ],

    // Proxy list for request manager
    'custom_proxy_list' => ROOT_PATH . '/private/ru_proxy_list.txt',

    'files_path' => ROOT_PATH . '/public/temp/',

    'graphite' => [
        'collector' => 'graphite',
        'host'      => 'graphite.smart-crowd.ru',
        'port'      => '2023',
    ],

    'cy_regions' => [
        'NNovgorod',
        'Kirov'
    ]
]);
