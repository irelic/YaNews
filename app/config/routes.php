<?php

if (CDI()->config->stage == 'maintenance') {
    $router = new Phalcon\Mvc\Router\Annotations(false);
    $router->setDefaults(['controller' => 'index', 'action' => 'maintenance' ]);

    return $router;
}

/**
 * Pass true - in development mode, otherwise false
 */
$router = new Phalcon\Mvc\Router\Annotations(false);

$router->removeExtraSlashes(true);

$router->setDefaults(['controller' => 'index', 'action' => 'route404' ]);
$router->notFound(   ['controller' => 'index', 'action' => 'route404' ]);

$panel = PanelResolver::getPanelName();

if ($panel == 'admin') {
    $router->add('/'            , ['controller' => 'index', 'action' => 'index']);
    $router->add('/index'       , ['controller' => 'index', 'action' => 'index']);
    $router->add('/items'       , ['controller' => 'index', 'action' => 'index']);
    $router->add('/sources'     , ['controller' => 'index', 'action' => 'index']);
    $router->add('/items_region', ['controller' => 'index', 'action' => 'index']);
    $router->add('/cy'          , ['controller' => 'index', 'action' => 'index']);

} elseif ($panel == 'client') {
    $router->add('/'            , ['controller' => 'link', 'action' => 'index']);
} elseif ($panel == 'backend') {
    $router->add('/'            , ['controller' => 'index', 'action' => 'backend']);
}

/* Служебные роуты */
$router->add('/beanstalk'                 , ['controller' => 'stats', 'action' => 'beanstalk']);
$router->add('/beanstalk/{tube}'          , ['controller' => 'stats', 'action' => 'beanstalk']);
$router->add('/beanstalk/{tube}/clear'    , ['controller' => 'stats', 'action' => 'clearTube']);

//
/* Роуты на апи через док. блоки */
$router->addResource('App\Controllers\Api\ItemsApi');
$router->addResource('App\Controllers\Api\RegionsApi');
$router->addResource('App\Controllers\Api\LinkApi');
$router->addResource('App\Controllers\Api\CyApi');

return $router;