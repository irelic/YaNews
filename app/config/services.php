<?php

use helper\ConfigHelper;
use \Phalcon\Loader;
use \Phalcon\DI\FactoryDefault;
use \Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Twig;
use \storage\RedisCache;
use \helper\Cache;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = empty($di) ? new FactoryDefault() : $di;

/**
 * Web app DI. Not used in CLI mode
 * @param FactoryDefault $di
 */
function loadWebDI (FactoryDefault $di)
{
    /**
     * Registering the view component
     *
     * @param {\Phalcon\Config} $config array
     */
    $di->set('view', function() {
        $view = new \Phalcon\Mvc\View();
        $view->registerEngines([
            '.phtml' => 'Phalcon\Mvc\View\Engine\Php',
            '.twig'  => function($view, $di) {
                $twig   = new Twig($view, $di, []);
                $jsVarFilter = new \Twig_SimpleFilter('jsVar', function ($string, $name) {
                        $string = json_encode($string);
                        $string = "<script>var " . $name . "=" . $string . ";</script>";
                        return $string;
                    }, ['is_safe' => ['html']]);
                $engine = $twig->getTwig();
                $engine->addFilter($jsVarFilter);

                return $twig;
            }
        ]);
        $view->setViewsDir(CDI()->config->application->views);

        return $view;
    });

    $di->set('router', function() { return (require (CDI()->config->application->app . '/config/routes.php')); });

    /**
     * The URL component is used to generate all kind of urls in the application
     */
    $di->setShared('url', function () {
        $url = new UrlResolver();
        $url->setBaseUri( CDI()->config->application->baseUri );
        return $url;
    });

    $di->setShared('dispatcher', function() use ($di) {
        $dispatcher = new \Phalcon\Mvc\Dispatcher();
        $dispatcher->setDefaultNamespace('App\Controllers');
        //We listen for events in the dispatcher using the Security plugin
        $eventsManager = $di->getShared('eventsManager');

        $eventsManager->attach('dispatch:beforeException', function($event, $dispatcher, $exception){
            $controller = $dispatcher->getActiveController();
            //Api exceptions interceptor
            if ($controller instanceof App\Controllers\Api\ApiControllerBase) {
                $controller->showError($exception->getMessage());
                exit();
            }
            throw $exception;
        });
        //Bind the eventsManager to the view component
        $dispatcher->setEventsManager($eventsManager);

        return $dispatcher;
    });

}

######## COMMON DI ########

$di->set('mongo', function() {
    $stage   = ConfigHelper::getStageStatus();
    $db_name = CDI()->config->mongo->$stage->database;
    return CDI()->mongoConnection->selectDb($db_name);
});
$di->set('cache', function() {
    $client = CDI()->clientResolver->getClient();
    $prefix = empty($client) ? "" : $client."_";
    $redis  = CDI()->redis->getInstance();
    return new Cache($redis, $prefix);
});

$di->setShared('mongoConnection', function(){
    $mongoCon = new storage\MongoStorage();
    return $mongoCon->getConnection();
});

$di->setShared('clientResolver'   , function() { return new ClientResolver(); });

$di->setShared('devLog'           , function() { return new Logger(CDI()->config->log->dev); });
//$di->setShared('profileLog'       , function() { return new Logger(CDI()->config->log->profile); });

$di->setShared('redis'            , function() { return new RedisCache(CDI()->config->redis->host, CDI()->config->redis->port); });
$di->setShared('collectionManager', function() { return new Phalcon\Mvc\Collection\Manager(); });

$di->setShared('graphite', function(){
    $parameters = CDI()->config->graphite->toArray();
    $collector  = CDI()->config->graphite->collector;
    $graphite   = \Beberlei\Metrics\Factory::create($collector, $parameters);
    return $graphite;
});

if ($config->stage_detection)
    $config->stage = ConfigHelper::getStageByHostname();

$di->setShared('config', $config);

$di->setShared('migrator', function() use ($di) {
    $collection = CDI()->mongo->selectCollection('migrations');
    $options['directory'] = ROOT_PATH . "/migrations";
    return new \migrator\Migrator($collection, $options);
});

/**
 * Global Dependency injector container
 * @return DIWrapper
 */
function CDI(){
    global $di;
    return DIWrapper::getInstance($di);
}
