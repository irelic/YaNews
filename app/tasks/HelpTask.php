<?php
/**
 * Basic helper for cli operations
 *
 * @author :  ignat
 * @date   :  21.07.14 10:16
 */

class HelpTask extends MainTask{

    public function mainAction(){

        echo "\nUsage:  php private/cli.php <OPTION>";
        echo "\nOptions:";
        echo "\n\tanalyze - send to Analyzer complex messages link";
        echo "\n\tfilter  - start filter worker";
        echo "\n\tgrabber - start grabber worker";
        echo "\n\tprocess - start post production process worker";
        echo "\n\n";

    }

} 