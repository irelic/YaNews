<?php

/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 25.01.16
 * Time: 13:24
 */

class ClearredisTask extends MainTask
{

    private static $keys = [
        'proxylist',
        'yandexregions'
    ];

    public function mainAction()
    {
        echo "Очистка кэша: \n";

        foreach (self::$keys as $key) {
            echo "$key\n";
            echo CDI()->cache->deleteCache($key);
            echo "\n";
        }
    }

}