<?php

use Phalcon\Cli\Task;

/**
 * @author : komrakov
 * @date   : 21.09.15 16:48
 */
class MigrationTask extends Task
{

    /**
     * Help
     */
    public function mainAction()
    {
        echo "### Доступные команды ###\n";
        echo "run      # запуск миграций\n";
        echo "reset    # откат всех сделанных миграций\n";
        echo "rollback # откат последней сделанной миграции\n";
        echo "refresh  # перезапуск всех имеющихся миграций\n";
        echo "status   # текущий статус миграций\n";
        echo "make     # создание новой миграции\n";
    }

    /**
     * Running All Outstanding Migrations
     */
    public function runAction()
    {
        CDI()->migrator->run();
    }

    /**
     * Rollback all migrations
     */
    public function resetAction()
    {
        CDI()->migrator->reset();
    }

    /**
     * Rollback The Last Migration
     */
    public function rollbackAction()
    {
        CDI()->migrator->rollback();
    }

    /**
     * Rollback all migrations and run them all again
     */
    public function refreshAction()
    {
        CDI()->migrator->reset();
        CDI()->migrator->run();
    }

    /**
     * Displays current migration status
     */
    public function statusAction()
    {
        CDI()->migrator->status();
    }

    /**
     * Create new empty migration file
     *
     * @example php private/cli.php migration make myNewMigration
     *
     * @param $name
     */
    public function makeAction($name)
    {
        CDI()->migrator->make($name);
    }

}