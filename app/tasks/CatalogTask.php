<?php
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.03.15
 * Time: 16:46
 */
use yandex\YandexCatalog;

class CatalogTask extends MainTask
{
    public function mainAction($startPage = 1, $limit = false)
    {
        $ya = new YandexCatalog();
        $ya->downloadCatalog($startPage, $limit);
    }
}
