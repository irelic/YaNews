<?php use models\Regions;
use queue\QueueFactory;
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.11.15
 * Time: 10:09
 */

class RegionsTask extends MainTask
{

    public function mainAction()
    {
        $regions = Regions::find();
        foreach($regions as $region) {
            QueueFactory::getQueue('process_regions')->send($region->toArray());
        }
    }

    /**
     * Запуск генерации задач в бесконечном цикле
     */
    public function daemonAction()
    {
        echo "Запуск демона\n";
        while (true) {
            echo date(DATE_RFC1036) . "\n";
            $this->mainAction();
            sleep(300);
        }
    }
}