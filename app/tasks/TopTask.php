<?php
/**
 * Basic helper for cli operations
 *
 * @author :  ignat
 * @date   :  21.07.14 10:16
 */

use queue\QueueFactory;
use yandex\Top;
use yandex\YandexRegions;

class TopTask extends MainTask
{
    /**
     * @return bool
     */
    public function mainAction()
    {
        $queue = QueueFactory::getQueue('process_rating');
        $top = new Top();
        $items = $top->getTop();
        $cy_regions = CDI()->config->cy_regions;

        foreach ($cy_regions as $region) {
            $items = array_merge($items, $top->getTop($region));
        }

        if (empty($items)) {
            CDI()->devLog->log('no top links were extracted', 'error');
            return false;
        }

        foreach ($items as $item) {
            /* @var $item \models\Items */
            if (!$this->checkInCache($item->getLink(), $item->getPosition(), $item->getRegion())) {
                $item->setNew(true);
            }

            $item = json_decode(json_encode($item), true);

            Graphite::log('grabber_tasks.sum', 1);
            
            $queue->send($item);
        }
    }


    /**
     * Запуск генерации задач в бесконечном цикле
     */
    public function daemonAction()
    {
        echo "Запуск демона\n";
        while (true) {
            echo date(DATE_RFC1036) . "\n";
            $this->mainAction();
            sleep(60);
        }
    }

    /**
     * @param $link
     * @param $position
     * @param null $region
     * @return bool
     */
    private function checkInCache($link, $position, $region = null)
    {
        $key = 'links_'.$link;
        if ($region) {
            $key = $key . '_' . $region;
        }
        $cache = CDI()->cache->getKey($key);
        if ($cache != false) {
            return true;
        }

        CDI()->cache->setEx($key, $position, 2*24*60*60);

        return false;
    }


} 