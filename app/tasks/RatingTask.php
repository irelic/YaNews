<?php
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 16.03.15
 * Time: 18:39
 */

use queue\AbstractJob;
use queue\QueueFactory;
use yandex\Ratings;

class RatingTask extends MainTask
{
    public function mainAction()
    {
        QueueFactory::getQueue('process_rating')->processJobs([$this, 'processRating']);
    }

    public function processRating(AbstractJob $job)
    {
        $msgContainer = $job->getData();
        $container = Ratings::processRating($msgContainer);
        $container->saveMessage();
    }

}