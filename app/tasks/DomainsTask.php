<?php use queue\AbstractJob;
use queue\QueueFactory;
use yandex\CategoryDomains;
use yandex\Top;

/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.11.15
 * Time: 10:09
 */

class DomainsTask extends MainTask
{

    public function mainAction()
    {
        QueueFactory::getQueue('process_regions')->processJobs([$this, 'processRegion']);
    }

    /**
     * @param AbstractJob $job
     */
    public function processRegion(AbstractJob $job)
    {
        $region = $job->getData();
        $top    = (new Top())->getRegionTop($region['string_id']);
        foreach($top as $link) {
            CategoryDomains::process($link, $region);
        }
    }

}