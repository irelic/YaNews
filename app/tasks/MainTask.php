<?php

use Phalcon\CLI\Task;

/**
 * Main CLI Task
 *
 * @package     base-app
 * @category    Task
 * @version     2.0
 */
class MainTask extends Task
{
    /**
     * Initialize
     *
     * @package     base-app
     * @version     2.0
     */
    public function initialize(){}

    /**
     * Main Action - display available tasks
     *
     * @package     base-app
     * @version     2.0
     */
    public function mainAction()
    {
        echo "-- CLI tasks --\n";
        foreach (new \DirectoryIterator(ROOT_PATH . '/app/tasks') as $file) {
            if ($file->isDot() || $file->getBasename('.php') == 'MainTask') {
                continue;
            }
            $task = $file->getBasename('.php');
            echo "  " . strtolower(strstr($task, 'Task', true)) . "\n";
        }
    }

    /**
     * Not found Action
     *
     * @package     base-app
     * @version     2.0
     */
    public function notFoundAction()
    {
        echo "CLI :: Task not found!\n";
    }

}