<?php
/**
 * Class to manage handle models mongo connections
 *
 * @author :  ignat
 * @date   :  18.09.14 12:44
 */

namespace models;

use Phalcon\Mvc\Collection;

class BaseCollection extends Collection{

    /**
     * Reset connection to model
     * @return \MongoDb
     */
    public function getConnection(){
        return CDI()->mongo;
    }

} 