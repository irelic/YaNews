<?php namespace models;
use validators\Required;
use validators\Unique;

/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.05.16
 * Time: 10:35
 */

class Tcy extends BaseCollection
{
    
    public $_id;
    public $domain;
    public $tcy;
    public $last_update;

    public static $tcy_ttl = 2592000; //30 days

    public static $_collection = 'tcy';

    public function onConstruct()
    {
        $this->setSource(self::$_collection);
        CDI()->mongo->selectCollection( self::$_collection )->ensureIndex(['domain' => 1]);
    }

    public function validation()
    {
        $this->validate(new Required([ "fields"  => [ 'domain', 'tcy', 'last_update'] ]));

        return $this->validationHasFailed() != true;
    }

    /**
     * saves without duplicates
     * @param $domain
     * @param $rating
     * @return bool
     */
    public static function saveTcy($domain, $rating)
    {
        if (self::count([['domain' => $domain]])) {
            return false;
        }

        $model = new self();
        $model->domain = $domain;
        $model->tcy = (int)$rating;

        return $model->save();
    }

    /**
     * @param $domain
     * @return Tcy|false
     */
    public static function findByDomain($domain)
    {
        return self::findFirst([['domain' => $domain]]);
    }

    /**
     * @param $domain
     * @return mixed
     */
    public static function stripDomain($domain)
    {
        $domain = strtolower(str_replace(['www.', 'http://', 'https://'], '', $domain));

        return $domain;
    }

    /**
     * @return bool
     */
    public function isOld()
    {
        if ((time() - $this->last_update) > self::$tcy_ttl) {
            return true;
        }
        
        return false;
    }

    public function beforeValidation()
    {
        $this->last_update = time();

        return true;
    }

}