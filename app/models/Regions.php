<?php

namespace models;


use validators\Required;

class Regions extends BaseCollection
{

    public $_id;
    public $string_id;
    public $text;

    public static $_collection = 'regions';

    public function onConstruct()
    {
        $this->setSource(self::$_collection);
    }

    public function initialize()
    {
        $this->useImplicitObjectIds(false);
    }


    public function validation()
    {
        $this->validate(new Required([ "fields"  => [ 'string_id', 'text'] ]));

        return $this->validationHasFailed() != true;
    }

}
