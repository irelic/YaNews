<?php namespace models;
use Phalcon\Annotations\Adapter\Base;
use validators\Required;

/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.11.15
 * Time: 15:21
 */

class Sources extends BaseCollection
{
    public $region_id;
    public $url;
    public $domain;
    public $time;
    public $new = false;

    public static $_collection = 'sources';

    public function onConstruct()
    {
        $this->setSource(self::$_collection);
        CDI()->mongo->selectCollection( self::$_collection )->ensureIndex(['region_id' => 1, 'time' => 1, 'domain' => 1]);
    }

    /**
     * @param $region_id
     * @param null $from
     * @param null $to
     * @param null|int $sort_field
     * @param null|int $sort_order
     * @return array
     */
    public function getSources($region_id, $from = null, $to = null, $sort_field = null, $sort_order = -1)
    {
        $query    = ['region_id' => (int)$region_id];

        if (!empty($from)) {
            $query['time']['$gte'] = (int)$from;
        }
        if (!empty($to)) {
            $query['time']['$lt'] = (int)$to;
        }

        $query = [
            [ '$match' => $query ],
            [ '$group' => [
                '_id' => ['domain' => '$domain'],
                'categories' => [
                    '$addToSet' => '$url',
                ],
                'total' => [
                    '$sum' => 1
                ],
                'new' => [
                    '$sum' => [ '$cond' => [ ['$eq' => ['$new', true] ], 1, 0 ] ]
                ]
            ]],
            [ '$project' => [
                'domain' => '$_id.domain',
                '_id'    => 0,
                'total'  => 1,
                'new'    => 1,
                'categories_count' => [ '$size' => '$categories' ]
            ]]
        ];

        if ($sort_field) {
            $query[] = ['$sort' => [$sort_field => (int)$sort_order]];
        }

        $sources = CDI()->mongo->command(['aggregate' => self::$_collection, 'pipeline' => $query, 'allowDiskUse' => true], ['socketTimeoutMS' => 150000]);

        return $sources['ok'] == 1 ? $sources['result'] : [];
    }

    /**
     * @return bool
     */
    public function validation()
    {
        $this->validate(new Required([ "fields"  => [ 'region_id', 'time', 'domain', 'url' ] ]));
        return $this->validationHasFailed() != true;
    }

    public function aggregateToCsv($from, $to, $region_id, $sort_field = null, $sort_order = 1)
    {
        $items = $this->getSources($region_id, $from, $to, $sort_field, $sort_order);
        $data = ['Домен;Упоминания при первом проходе;Общее кол-во упоминаний;Кол-во категорий'];

        foreach ($items as $item) {
            $data[] = implode(';', [ $item['domain'], $item['new'], $item['total'], $item['categories_count'] ]);
        }

        $data = implode("\n", $data);

        return $data;
    }

}