<?php
/**
 * Model for Message from YandexNews
 *
 * @author :  ignat
 * @date   :  16.03.15 16:19
 */


namespace models;

use helper\Model;
use validators\Required;


class Items extends BaseCollection
{

    public $_id;
    public $time;
    public $link;
    public $text;
    public $position;
    public $rating;
    public $hosts;
    public $new = false;
    public $region = null;

    public static $_collection = 'items';

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return mixed
     */
    public function getHosts()
    {
        return $this->hosts;
    }

    /**
     * @param mixed $hosts
     */
    public function setHosts($hosts)
    {
        $this->hosts = $hosts;
    }

    /**
     * @return bool
     */
    public function getNew()
    {
        return $this->new;
    }

    /**
     * @param bool $new
     */
    public function setNew($new)
    {
        $this->new = $new;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region_id
     */
    public function setRegion($region_id)
    {
        $this->region = $region_id;
    }

    public function makeFromArray(array $array)
    {
        $keys = array_keys($array);
        foreach ($keys as $key) {
            $setter = 'set'.ucfirst($key);
            if (method_exists($this, $setter)) {
                $this->$setter($array[$key]);
            }
        }

        return $this;
    }



    public function onConstruct()
    {
        $this->setSource(self::$_collection);
        CDI()->mongo->selectCollection( self::$_collection )->ensureIndex(['time' => 1]);
    }


    public function saveMessage(array $message=null)
    {
        try {
            if (!empty($message)) {
                $message = Model::getModel(__CLASS__, $message);
            } else {
                $message = $this;
            }

            if (CDI()->mongo->selectCollection(self::$_collection)->count(['_id' => $message->getId()]) > 0) {
                throw new \Exception("Trying to save message already existing in database: " . json_encode($message));
            }

            if (CDI()->mongo->selectCollection(self::$_collection)->count(['time' => $message->getTime(), 'link' => $message->getLink(), 'position' => $message->getPosition()]) > 0) {
                throw new \Exception("Link already exists on this position at the time: " . json_encode($message));
            }

            if ($message->save() == false) {
                throw new \Exception(implode("; ", $message->getMessages()));
            }

            self::log_message($message);

            return $message->getId();
        } catch (\Exception $e) {
            CDI()->devLog->log( $e->getMessage(), "error" );

            return false;
        }
    }

    public function getItems($from=null, $to=null, $limit=100, $skip=0, $region=null)
    {
        $time     = $from ? (int)$from : time()-7*24*60*60;
        $query    = ['time' => ['$gte' => $time]];
        if (!empty($to)) {
            $query['time']['$lt'] = (int)$to;
        }

        if ($region) {
            $query['region'] = (int)$region;
        } else {
            $query['region'] = null;
        }
        
        $result = [];
        $messages = CDI()->mongo->selectCollection(self::$_collection)->aggregate([
            [ '$match' => $query ],
            [ '$group' => ['_id' => '$time', 'items' => ['$push' => '$$ROOT' ]]],
            [ '$sort'  => [ '_id' => -1 ]],
            [ '$skip'  => (int)$skip ],
            [ '$limit' => (int)$limit ],
        ], ['allowDiskUse' => true]);
        if (isset($messages['result'])) {
            foreach ($messages['result'] as $key => $element) {
                if (count($element['items']) < 5) {
                    continue;
                }
                usort($element['items'], [$this, 'compare']);
                $result[(int)$key]['id'] = (int)$element['_id'];
                $result[(int)$key]['items'] = array_splice($element['items'], 0, 5);
            }
        }

        return array_values($result);
    }

    private function compare($a, $b)
    {
        if ($a['position'] == $b['position']) {
            return 0;
        }
        return ($a['position'] < $b['position']) ? -1 : 1;
    }

    public function getItemsCount($from=null, $to=null, $region=null)
    {
        $time     = $from ? (int)$from : time()-7*24*60*60;
        $query    = ['time' => ['$gte' => $time]];
        if (!empty($to)) {
            $query['time']['$lt'] = (int)$to;
        }
        if ($region) {
            $query['region'] = (int)$region;
        } else {
            $query['region'] = null;
        }
        $count = 0;
        $messages = CDI()->mongo->selectCollection(self::$_collection)->aggregate([
            [ '$match' => $query ],
            [ '$group' => ['_id' => '$time']],
        ], ['allowDiskUse' => true]);

        if (isset($messages['result'])) {
            $count = count($messages['result']);
        }

        return $count;
    }

    public function aggregateToCsv($from, $to)
    {
        $items = $this->getItems($from, $to);
        $data  = ['Время; 1; 2; 3; 4; 5;'];

        foreach ($items as $item) {
            $rating = [date('d-m-Y H:i', $item['id'])];
            foreach ($item['items'] as $val) {
                $rating[] = $val['rating'];
            }
            $data[] = implode(';', $rating);
        }
        $data = implode("\n", $data);

        return $data;
    }

    public function validation()
    {
        $this->validate(new Required([ "fields"  => [ 'text', 'rating', 'time', 'position' ] ]));

        return $this->validationHasFailed() != true;
    }

    private static function log_message(Items $message)
    {
        if (!empty($message->region)) {
            \Graphite::log('items_region.' . $message->region . '.sum', 1);
        } else {
            \Graphite::log('items.sum', 1);
        }
    }
}
