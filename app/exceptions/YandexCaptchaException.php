<?php namespace App\Exceptions;
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 16.05.16
 * Time: 10:48
 */

class YandexCaptchaException extends \Exception
{

    protected $message = 'Yandex banned this request';

    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        if (!$message) {
            $message = $this->message;
        }
        
        parent::__construct($message, $code, $previous);
    }
}