<?php
/**
* Short product info
*
* @author :  komrakov
* @date   :  25.11.14 10:35
*/

namespace queue;
use Pheanstalk\Pheanstalk;

/**
 * Class QueueException
 * @Code
 * 0: Increment priority and resend task in queue with 60 seconds delay. Bury job after 3 hours
 * @package queue
 */
class QueueException extends \Exception {

    public static function handleException(QueueException $e, AbstractJob $job){
        switch ($e->getCode()) {
            case 0:
                $priority = $job->getJobPriority( $job->getId() );
                if ($priority > Pheanstalk::DEFAULT_PRIORITY + 10) { // Bury after 10 minutes
                    $job->bury( $job->getId() );
                    break;
                }
                $job->release( $job->getId(), $priority + 1, 60 );
                break;
            default:
                throw new \Exception($e);
        }
    }

}