<?php

namespace queue;

/**
 * User: komrakov
 * Date: 08.08.14
 * Time: 15:58
 */
class QueueFactory {

    /**
     * @param string $tube
     * @param string $queue Query module name
     *
     * @return \queue\AbstractQueue
     */
    public static function getQueue($tube = "default", $queue = "pheanstalk")
    {
        $options    = CDI()->config->beanstalk->toArray();
        $class_name = "queue\\$queue\\Queue";

        return new $class_name($tube, $options);
    }
}