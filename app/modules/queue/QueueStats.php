<?php
/**
 * Statistics component for queue
 *
 * @author :  ignat
 * @date   :  27.10.14 17:20
 */


namespace queue;


class QueueStats {

    public static $maxJobs = 500000;

    /**
     * Get tubes list
     * @return array
     */
    public static function getTubes()
    {
        return QueueFactory::getQueue()->getInnerQueue()->listTubes();
    }

    /**
     * Clear passed tube (or all tubes at once)
     *
     * @param string|null $name
     */
    public static function clearTube($name = null){
        QueueFactory::getQueue($name)->clearTube();
    }

    /**
     * Get statistics of all available tubes
     * @return array
     */
    public static function getStats(){

        $stats = [];
        $queue = QueueFactory::getQueue()->getInnerQueue();

        foreach(self::getTubes() as $i => $tube){
            $data = $queue->statsTube($tube);
            $stats[$i] = [
                'name'     => $tube,
                'ready'    => $data['current-jobs-ready'],
                'reserved' => $data['current-jobs-reserved'],
                'delayed'  => $data['current-jobs-delayed'],
                'buried'   => $data['current-jobs-buried'],
                'total'    => $data['total-jobs']
            ];
        }
        return $stats;
    }

} 