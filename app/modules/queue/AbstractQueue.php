<?php

namespace queue;

use Pheanstalk\Pheanstalk;

/**
 * Created by PhpStorm.
 * User: komrakov
 * Date: 8/7/14
 * Time: 11:58 PM
 */
abstract class AbstractQueue {

    /**
     * @var Pheanstalk
     */
    protected $innerQueue;
    protected $tube = "default";

    public function getInnerQueue(){
        return $this->innerQueue;
    }

    /**
     * Setting up $this->innerQueue and $this->tube
     *
     * @param       $tube string | null
     * @param array $options
     */
    abstract public function __construct($tube = null, $options = []);

    /**
     * @param $recursive bool
     * @return \Generator | AbstractJob[]
     */
    abstract public function listen($recursive = true);


    /**
     * @param $message mixed Some useful data to put in query
     *
     * @return mixed JobId
     */
    abstract public function send($message);

    abstract public function delete($job);

    abstract public function jobExist($jobId);

}