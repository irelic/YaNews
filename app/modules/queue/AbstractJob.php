<?php
/**
 * Created by PhpStorm.
 * User: komrakov
 * Date: 08.08.14
 * Time: 10:54
 */

namespace queue;


abstract class AbstractJob
{

    protected $innerQueue;
    protected $_id;
    protected $data;

    /**
     * Setup $this->innerQueue
     *
     * @param       $innerQueue mixed Inner queue object
     * @param array $options
     */
    abstract public function __construct($innerQueue, $options = []);

    public function getId(){
        return $this->_id;
    }

    public function getData(){
        return $this->data;
    }

    abstract public function reserve();
    abstract public function release($id, $priority = null, $delay = null);
    abstract public function bury($id);
    abstract public function getJobPriority($id);

}