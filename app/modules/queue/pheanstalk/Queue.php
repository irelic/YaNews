<?php

namespace queue\pheanstalk;

use Pheanstalk\Pheanstalk;
use queue\AbstractJob;
use queue\AbstractQueue;
use queue\QueueException;

/**
 * Created by PhpStorm.
 *
 * User: komrakov
 * Date: 8/8/14
 * Time: 12:15 AM
 */
class Queue extends AbstractQueue {

    protected $options;

    protected $listenerDelay;
    protected $reconnectTimeout;
    protected $maxReleases;

    protected $default_priority;
    protected $default_delay;
    protected $default_ttr;
    protected $default_port;

    public function __construct($tube = "default", $options = [])
    {
        $this->options = $options;

        $this->listenerDelay    = isset($options['listenerDelay'])    ? $options['listenerDelay']    : 5;
        $this->reconnectTimeout = isset($options['reconnectTimeout']) ? $options['reconnectTimeout'] : 10;
        $this->maxReleases      = isset($options['maxReleases'])      ? $options['maxReleases']      : 3;

        $this->default_priority = isset($options['default_priority']) ? $options['default_priority'] : Pheanstalk::DEFAULT_PRIORITY;
        $this->default_delay    = isset($options['default_delay'])    ? $options['default_delay']    : Pheanstalk::DEFAULT_DELAY;
        $this->default_ttr      = isset($options['default_ttr'])      ? $options['default_ttr']      : Pheanstalk::DEFAULT_TTR;
        $this->default_port     = isset($options['default_port'])     ? $options['default_port']     : Pheanstalk::DEFAULT_PORT;

        $this->tube = $tube;

        $this->connect($options);
    }

    protected function connect($options = [])
    {
        // Default connection options
        $default_options = [
            'host'              => "127.0.0.1",
            'port'              => $this->default_port,
            'connectTimeout'    => null,
            'connectPersistent' => false,
        ];
        $options = array_merge($default_options, $options);

        // Close current Pheanstalk connection on innerQueue->__destruct
        if ( !is_null($this->innerQueue) )
            $this->innerQueue = null;

        try {
            $this->innerQueue = new Pheanstalk($options['host'], $options['port'], $options['connectTimeout'], $options['connectPersistent']);
            $this->innerQueue->useTube($this->tube);
        } catch (\Exception $e) {
            CDI()->devLog->log("Beanstalk attempt to reconnect in " . $this->reconnectTimeout . " seconds");
            sleep( $this->reconnectTimeout );
            $this->connect();
        }
    }

    public function send($data, $priority = null, $delay = null, $ttr = null)
    {
        $priority = is_null($priority) ? $this->default_priority : $priority;
        $delay    = is_null($delay)    ? $this->default_delay    : $delay;
        $ttr      = is_null($ttr)      ? $this->default_ttr      : $ttr;

        return $this->innerQueue->useTube($this->tube)->put( json_encode($data), $priority, $delay, $ttr );
    }

    public function listen($recursive = true)
    {
        $this->innerQueue->watchOnly($this->tube);
        do {
            try {
                $job = new Job( $this->innerQueue, $this->options );
                while ( $job->reserve() ) {
                    yield $job;
                }
                if ($recursive)
                    sleep( $this->listenerDelay );
            } catch (\Exception $e) {
                $message = $e->getMessage();
                if ( stristr($message, "Socket error 111") !== false ) {
                    CDI()->devLog->log($message);
                    $this->connect();
                } else {
                    throw new \Exception($e);
                }
            } finally {
                $job = null;
            }
        } while ( $recursive === true );
    }

    public function jobExist($id)
    {
        try {
            return in_array(
                $this->innerQueue->statsJob($id)['state'],
                ['reserved' , 'ready', 'urgent', 'delayed']
            );
        } catch (\Exception $e) {
            $message = $e->getMessage();
            if ( stristr($message, "Server reported NOT_FOUND") !== false )
                return false;

            throw new \Exception($e);
        }
    }

    /**
     * @param AbstractJob $job
     * @return bool
     */
    public function delete($job)
    {
        try {
            $this->innerQueue->delete($job);
            return true;
        } catch (\Exception $e) {
            $id = $job->getId();
            CDI()->devLog->log($e->getMessage());
            CDI()->devLog->log("Error on delete Beanstalk Job :: $id");
            return false;
        }
    }

    /**
     * Listens selected tube and sends Job to Callable function
     * @param callable $callback
     * @param          $recursive bool
     * @throws \Exception
     */
    public function processJobs(Callable $callback, $recursive = true)
    {
        foreach( $this->listen($recursive) as $job){
            try {
                $callback($job);
                $this->delete($job);
            } catch (QueueException $e) {
                QueueException::handleException($e, $job);
            } catch (\Exception $e) {
                CDI()->devLog->log("Error on processing job \n" . $e->getMessage() . "\n" . $e->getTraceAsString());
                $priority = $job->getJobPriority( $job->getId() );
                $action = ($priority >= $this->default_priority + $this->maxReleases) ? "bury" : "release";
                $job->$action( $job->getId(), $priority + 1 );
            }
        }
    }

    /**
     * Clears current used tube
     * @return int Deleted jobs count
     */
    public function clearTube()
    {
        $deleted = 0;
        /**
         * @var $job \queue\AbstractJob
         */
        $delete = function($job) use (&$deleted) {
            $this->delete($job);
            $deleted++;
        };
        $this->processJobs($delete, false);

        return $deleted;
    }

} 