<?php
/**
 * Created by PhpStorm.
 * User: komrakov
 * Date: 08.08.14
 * Time: 10:54
 */

namespace queue\pheanstalk;

use queue\AbstractJob;
use Pheanstalk\Pheanstalk;

class Job extends AbstractJob
{

    protected $jobReserveTimeout;

    protected $default_priority;
    protected $default_delay;

    /**
     * @param       $innerQueue Pheanstalk
     * @param array $options
     */
    public function __construct( $innerQueue, $options = [] )
    {
        $this->innerQueue = $innerQueue;

        $this->jobReserveTimeout = isset($options['jobReserveTimeout']) ? $options['jobReserveTimeout'] : 0;

        $this->default_priority  = isset($options['default_priority'])  ? $options['default_priority']  : Pheanstalk::DEFAULT_PRIORITY;
        $this->default_delay     = isset($options['default_delay'])     ? $options['default_delay']     : Pheanstalk::DEFAULT_DELAY;
    }

    public function reserve()
    {

        if ( $innerJob = $this->innerQueue->reserve( $this->jobReserveTimeout ) ) {
            $this->_id = $innerJob->getId();
            $this->data = json_decode( $innerJob->getData(), true );
            return $this;
        } else {
            return false;
        }
    }

    public function release($id, $priority = null, $delay = null)
    {
        $priority = is_null($priority) ? $this->default_priority : $priority;
        $delay    = is_null($delay)    ? $this->default_delay    : $delay;

        try {
            $innerJob = $this->innerQueue->peek($id);
            $this->innerQueue->release($innerJob, $priority, $delay);
        } catch (\Exception $e) {
            CDI()->devLog->log("Error on release Beanstalk Job :: $id");
        }
    }

    public function bury($id, $priority = null)
    {
        $priority = is_null($priority) ? $this->default_priority : $priority;

        try {
            $innerJob = $this->innerQueue->peek($id);
            $this->innerQueue->bury($innerJob, $priority);
        } catch (\Exception $e) {
            CDI()->devLog->log("Error on bury Beanstalk Job :: $id");
        }
    }

    public function getJobPriority($id)
    {
        return $this->innerQueue->statsJob($id)['pri'];
    }

    /**
     * @return array
     */
    public function getTubesWatched()
    {
        return $this->innerQueue->listTubesWatched();
    }

}