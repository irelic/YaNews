<?php

namespace migrator;

/**
 * @author :  komrakov
 * @date   :  21.09.15 17:44
 */
interface IMigration {

    public function up();
    public function down();

}