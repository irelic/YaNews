<?php

namespace migrator;

/**
 * @author :  komrakov
 * @date   :  21.09.15 19:00
 */
class Migrator {

    public $directory;
    public $collection;
    public $stub;
    public $suffix;

    public function __construct(\MongoCollection $collection, array $options = []) {
        $this->collection = $collection;
        $this->directory  = empty($options['directory']) ? __DIR__                     : $options['directory'];
        $this->stub       = empty($options['stub'])      ? __DIR__ . "/migration.stub" : $options['stub'];
        $this->suffix     = empty($options['suffix'])    ? "Migration"                 : $options['suffix'];
    }

    /**
     * Running All Outstanding Migrations
     */
    public function run() {
        $files = $this->getFiles($this->directory);
        $files = $this->filterMigrationFiles($files);
        $db_files = $this->getDbMigrations();
        $migrations = array_diff($files, $db_files);
        $this->printMigrations($migrations, "### Run new migrations\n", "### No new migrations found\n");
        $this->runUp($migrations);
    }

    /**
     * Rollback all migrations
     */
    public function reset() {
        $migrations = $this->getDbMigrations();
        $this->printMigrations($migrations, "### Reset migrations\n", "### No migrations to reset\n");
        $this->runDown($migrations);
    }

    /**
     * Rollback The Last Migration
     */
    public function rollback() {
        $migrations = $this->getDbMigrations(1);
        $this->printMigrations($migrations, "### Rollback migration\n", "### No migrations to rollback\n");
        $this->runDown($migrations);
    }

    /**
     * Displays current migration status
     */
    public function status() {
        $db_files = $this->getDbMigrations();
        $this->printMigrations($db_files, "### Migrated\n", "### Migrated nothing\n");

        $files = $this->getFiles($this->directory);
        $files = $this->filterMigrationFiles($files);
        $migrations = array_diff($files, $db_files);
        $this->printMigrations($migrations, "### New migrations\n", "### No new migrations found\n");
    }

    /**
     * Create new empty migration file
     *
     * @example php private/cli.php migration make myNewMigration
     *
     * @param $name
     */
    public function make($name) {
        $class_name = self::studly($name) . $this->suffix;
        $file_name  = $this->getDatePrefix() . "_" . self::snake($name) . ".php";

        $migration_stub = $this->getStub();
        $migration_stub = $this->fillStub($class_name, $migration_stub);

        file_put_contents($this->directory . "/" . $file_name, $migration_stub);
    }

    protected function runUp($migrations) {
        $this->requireFiles($this->directory, $migrations);
        foreach($migrations as $migration) {
            $class = $this->resolve($migration);
            $class->up();
            $this->collection->insert(['_id' => $migration]);
        }
    }

    protected function runDown($migrations) {
        $this->requireFiles($this->directory, $migrations);
        foreach($migrations as $migration) {
            $class = $this->resolve($migration);
            $class->down();
            $this->collection->remove(['_id' => $migration]);
        }
    }

    protected static function printMigrations($migrations, $message, $empty_message = "") {
        echo empty($migrations) ? $empty_message : $message;
        foreach($migrations as $migration) {
            echo $migration . "\n";
        }
    }

    /**
     * Get all file names in directory
     *
     * @param string $dir
     *
     * @return array
     */
    protected function getFiles($dir) {
        $files_dirs = scandir($dir);
        $files      = array_diff($files_dirs, ['..', '.']);

        return $files;
    }

    /**
     * Filter array file names. Passing valid migration file names only
     *
     * @param array $files
     *
     * @return array
     */
    protected function filterMigrationFiles(array $files)
    {
        // Filter migration files by date prefix pattern
        $files = preg_grep('/^\d{4}_\d{2}_\d{2}_\d{6}.*\.php$/', $files);

        // Once we have the array of files in the directory we will just remove the
        // extension and take the basename of the file which is all we need when
        // finding the migrations that haven't been run against the databases.
        $files = array_map(function ($file) {
            return str_replace('.php', '', basename($file));
        }, $files);

        // Once we have all of the formatted file names we will sort them and since
        // they all start with a timestamp this should give us the migrations in
        // the order they were actually created by the application developers.
        sort($files);

        return $files;
    }

    /**
     * Get db migrations list sorted by _id (migration name) in reverse order
     *
     * @param int $limit
     *
     * @return array
     */
    protected function getDbMigrations($limit = 0) {
        $result = $this->collection->find([], ['_id' => true])->sort(['_id' => -1])->limit($limit);
        $result = iterator_to_array($result, false);
        $result = array_column($result, '_id');

        return $result;
    }

    /**
     * Require in all the migration files in a given path.
     *
     * @param  string $path
     * @param  array  $files
     *
     * @throws \Exception
     */
    protected function requireFiles($path, array $files)
    {
        foreach ($files as $file) {
            $full_path = $path . '/' . $file . '.php';
            if (!file_exists($full_path)) {
                throw new \Exception("Migration file not found");
            }

            require_once $full_path;
        }
    }

    /**
     * Resolve a migration instance from a file.
     *
     * @param  string  $file
     * @return object
     */
    protected function resolve($file)
    {
        $file = implode('_', array_slice(explode('_', $file), 4));

        $class = self::studly($file) . $this->suffix;

        return new $class;
    }

    /**
     * Get the date prefix for the migration.
     *
     * @return string
     */
    protected function getDatePrefix()
    {
        return date('Y_m_d_His');
    }

    /**
     * Add class name in migration stub file
     *
     * @param string $class
     * @param string $stub
     *
     * @return string
     */
    protected function fillStub($class, $stub) {
        $stub = str_replace("{{CLASSNAME}}", $class, $stub);

        return $stub;
    }

    /**
     * Return stub file content
     *
     * @return string | false
     */
    protected function getStub() {
        return file_get_contents($this->stub);
    }

    /**
     * Convert a string to snake case.
     *
     * @param  string  $value
     * @param  string  $delimiter
     * @return string
     */
    public static function snake($value, $delimiter = '_')
    {
        if (!ctype_lower($value)) {
            $value = strtolower(preg_replace('/(.)(?=[A-Z])/', '$1'.$delimiter, $value));
        }

        return $value;
    }

    /**
     * Convert a value to studly caps case.
     *
     * @param  string  $value
     * @return string
     */
    public static function studly($value)
    {
        $value = ucwords(str_replace(['-', '_'], ' ', $value));

        return str_replace(' ', '', $value);
    }

}