<?php
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 01.03.16
 * Time: 11:19
 */

class PanelResolver
{

    public static function getPanelName($host = null)
    {
        $host = !$host ? $_SERVER['HTTP_HOST'] : $host;
        $parts = explode('.', $host);

        $panelName = array_shift($parts);

        return $panelName;
    }

}