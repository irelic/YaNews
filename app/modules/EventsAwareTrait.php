<?php
use Phalcon\Events\EventsAwareInterface;

/**
 * Created by IntelliJ IDEA.
 * User: soldatenko
 * Date: 19.06.14
 * Time: 16:28
 */

trait EventsAwareTrait  {

    protected $_eventsManager;

    public function setEventsManager($eventsManager)
    {
        $this->_eventsManager = $eventsManager;
    }

    public function getEventsManager()
    {
        return $this->_eventsManager;
    }

    protected function fireEvent($tag,$data=null){
        $this->getEventsManager()->fire($tag,$this,$data);
    }

} 