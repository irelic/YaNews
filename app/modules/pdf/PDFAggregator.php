<?php namespace pdf;
use models\Regions;
use models\Sources;
use Phalcon\Mvc\View\Simple;
use yandex\Avatar;
use yandex\Ratings;
use yandex\YandexRegions;

/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 03.03.16
 * Time: 18:25
 */

class PDFAggregator
{

    private static $position_period = 'month';
    private static $sort_order = -1;

    public static function sources($type, $region_id, $from = null, $to = null, $sort_field = 'total')
    {
        $model = new Sources();
        $sort_field = $sort_field ? : 'total';
        $data['items']      = $model->getSources($region_id, $from, $to, $sort_field);
        $data['full_count'] = count($data['items']);
        $data['items']      = array_slice($data['items'], 0, 99);
        $data['sort_field'] = $sort_field;
        $data['date_from']  = date('d.m.Y', $from);
        $data['date_to']    = date('d.m.Y', $to-1);

        if ($type == self::$position_period) {
            $data['show_position_change'] = true;
            $month = 60*60*24*30;
            $items_before = $model->getSources($region_id, $from-$month, $to-$month, $sort_field);
            $items_before_prepared = [];
            foreach ($items_before as $i => $item) {
                $items_before_prepared[$item['domain']] = $item;
                $items_before_prepared[$item['domain']]['position'] = $i+1;
            }
        }

        foreach ($data['items'] as $i => $item) {
            $domain = $data['items'][$i]['domain'];
            $data['items'][$i]['rating'] = Ratings::getRating($domain);
            $data['items'][$i]['logo'] = Avatar::getFavicon($domain);
            if ($type == self::$position_period) {
                $data['items'][$i]['previous_position'] = isset($items_before_prepared[$domain]['position']) ? $items_before_prepared[$domain]['position'] : 0;
            }
        }

        $region = Regions::findById((int)$region_id);
        $data['region'] = !empty($region) ? $region->text : '';

        $view = CDI()->view->getRender('reports', 'sources', $data);
        define('_MPDF_TTFONTDATAPATH', CDI()->config->files_path. '/mpdf');
        $mpdf = new \mPDF('utf-8', 'A4', 10, 'GothamProBold', 0, 0, 0, 0, 0, 0);
        $mpdf->dpi = 300;
        $mpdf->charset_in = 'cp1251';
        $mpdf->WriteHTML($view);
        $filename = 'report.pdf';
        $dest = CDI()->config->files_path;
        $mpdf->Output($dest.$filename, 'F');

        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/pdf");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        return $filename;
    }

}