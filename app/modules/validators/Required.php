<?php
/**
 * User: soldatenko
 * Date: 16.06.14 11:41
 */
namespace validators;

use Phalcon\Mvc\Model\Validator;
use Phalcon\Mvc\Model\ValidatorInterface;

class Required extends Validator implements ValidatorInterface
{

    /**
     * @param \Phalcon\Mvc\EntityInterface $record
     * @return bool
     */
    public function validate(\Phalcon\Mvc\EntityInterface $record)
    {
        $fields = $this->getOption('fields');
        if (is_null($fields))
            $fields = [$this->getOption('field')];

        foreach ($fields as $field) {
            if (empty($record->$field)) {
                $this->appendMessage("Field " . $field . " is empty", $field, "Required");
                return false;
            }
        }
        return true;
    }

}