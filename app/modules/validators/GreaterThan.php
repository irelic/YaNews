<?php
/**
* Short product info
*
* @author :  komrakov
* @date   :  19.09.14 16:14
*/

namespace validators;

use Phalcon\Mvc\Model\Validator;
use Phalcon\Mvc\Model\ValidatorInterface;

/**
 * Class GreaterThan
 *
 * @package validators
 */
class GreaterThan extends Validator implements ValidatorInterface {

    /**
     * Return false with $message error if value lesser than target
     * @param \Phalcon\Mvc\EntityInterface $record
     *
     * @return bool
     */
    public function validate(\Phalcon\Mvc\EntityInterface $record)
    {
        $value   = $this->getOption('value');
        $target  = $this->getOption('target');
        $message = $this->getOption('message');

        if ($value > $target)
            return true;

        $this->appendMessage($message);
        return false;
    }

}