<?php
namespace validators;

use Phalcon\Mvc\Model\Validator;
use Phalcon\Mvc\Model\ValidatorInterface;

class Unique extends Validator implements ValidatorInterface
{

    /**
     * @param \Phalcon\Mvc\EntityInterface $record
     * @return bool
     */
    public function validate(\Phalcon\Mvc\EntityInterface $record)
    {
        $field = $this->getOption('field');

        if ($record->count(["field" => $record->readAttribute($field)])) {
            $this->appendMessage("The ".$field." must be unique", $field, "Unique");
            return false;
        }
        
        return true;
    }

}