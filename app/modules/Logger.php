<?php

Class Logger
{

    private $activeLog;
    private $logger;

    public function __construct($logFile, $force = false)
    {
        $this->activeLog = $force || CDI()->config->log->dev_active;
        try {
            $this->logger = new Phalcon\Logger\Adapter\File($logFile);
        } catch (\Exception $e) {
            $this->activeLog = false;
        }
    }

    /**
     * @param string $message
     * @param string $type ['debug', 'info', 'notice', 'warning', 'error', 'critical', 'alert', 'emergency']
     *
     * @return bool
     */
    public function log($message, $type = 'info')
    {
        $message = ltrim($message);
        if ($this->activeLog === true && $message != '') {
            try {
//                $this->toNewRelic($message, $type);
                if (is_object($this->logger)) $this->logger->$type($message);
            } catch (\Exception $e) {
                return false;
            }
        }
        return true;
    }

    public function __call($name, $arguments)
    {
        if ($this->activeLog === true)
        {
            if (method_exists($this->logger, $name)) {
                $this->log($arguments, $name);
            }
        }
    }

//    /**
//     * @param $message string
//     * @param $type    string
//     */
//    public function toNewRelic($message, $type)
//    {
//        $type = strtoupper($type);
//        $newRelicTypes = ['WARNING', 'ERROR', 'CRITICAL', 'ALERT', 'EMERGENCY'];
//        if ( in_array($type, $newRelicTypes) ) {
//            $message = $this->logger->getFormatter()->format($message, constant('\Phalcon\Logger::'.$type), time(), null);
//            \Newrelic::notice($message);
//        }
//    }

}