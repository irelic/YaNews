<?php namespace yandex;
use DOMDocument;

/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 16.05.16
 * Time: 10:42
 */

class YandexError
{

    /**
     * @param DOMDocument $dom
     * @return bool
     */
    public static function isContentEmpty(DOMDocument $dom)
    {
        return !$dom->textContent;
    }

    /**
     * @param DOMDocument $dom
     * @return bool
     */
    public static function isCaptcha(DOMDocument $dom)
    {
        return (bool)preg_match('/checkcaptcha/u', $dom->saveHTML());
    }

}