<?php

namespace yandex;

use \DOM;

/**
 * @author :  nikolaev
 * @date   :  20.01.2015
 */
class YandexRegions
{
    private $filename;

    public function __construct()
    {
        $this->filename = ROOT_PATH.'/private/yandex_regions.txt';
    }

    /**
     * parse regions list from yandex
     * @return array
     */
    public function processRegions()
    {
        $domain = 'http://news.yandex.ru';
        $dom = DOM::getDOM($domain.'/regions/index.html');
        $res = DOM::findByXPath('//*[@class ="regions"]//*[@class="regions__list"]//a', $dom);
        $regions = [];
        $i = 0;
        foreach ($res as $value) {
            $string_id = explode('/',$value->getAttribute('href'))[1];
            $regions[$i]['string_id'] = $string_id;
            $regions[$i]['text'] = $value->textContent;
            $dom = DOM::getDOM($domain.$value->getAttribute('href'));
            $res = DOM::findByXPath('//*[@class="stories__all"]/a', $dom);
            $geonews = explode('&',$res->item(0)->getAttribute('href'))[1];
            $id = substr($geonews, 8, strlen($geonews));
            $regions[$i]['id'] = $id;
            $i++;
        }

        return $regions;
    }


    /**
     * gets regions list from cache or file
     * @return array
     */
    public function getRegions()
    {
        if (($cache = CDI()->cache->getCache('yandexRegions', 'all')) !== false) {
            return $cache;
        }

        $result = json_decode($this->getFromFile(), true);
        if (!count($result)) {
            $result = $this->force();
        }

        CDI()->cache->setCache('yandexRegions', 'all', $result);

        return $result;
    }

    /**
     * parses regions and writes to file
     * @return array
     */
    public function force()
    {
        $result = $this->processRegions();
        $this->writeToFile(json_encode($result));
        return $result;
    }


    /**
     * @return string
     */
    public function getFromFile()
    {
        $fp = @fopen($this->filename, 'r');
        if (!$fp) {
            CDI()->devLog->log('Failed to open file:' . $this->filename);
            return '';
        }
        $data = @fread($fp, filesize($this->filename) );
        fclose($fp);
        return $data;
    }

    /**
     * @param $text
     * @return array
     */
    public function writeToFile($text)
    {
        $fp = @fopen($this->filename, 'w');
        if (!$fp) {
            CDI()->devLog->log('Failed to open file:' . $this->filename);
            return [];
        }
        @fwrite($fp, $text);
        fclose($fp);
    }


}
 