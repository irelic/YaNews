<?php
/**
 * Yandex Catalog Parser
 *
 * @author :  ignat
 * @date   :  16.03.15 18:09
 */

namespace yandex;

use \DOM;
use helper\RequestManager;
use yandex\CatalogItem;

class YandexCatalog
{
//    public $url = 'https://yaca.yandex.ru/';
//    public $url = 'https://yaca.yandex.ru/yca/cat/';

    /**
     * Rubric pages (all with pages)
     * @var string BASE_URL
     */
    const BASE_URL = 'https://yandex.ru/';

    private $url;

    private $rubricName = '';
    private $rubricLink = '';

    private $dbName = 'yandex_catalog';

    /**
     * @var \MongoDB
     */
    private $db;

    /**
     * @var \MongoCollection
     */
    private $coll;

    private $maxAttempts = 3;

    public function __construct()
    {
        $this->db = CDI()->mongoConnection->selectDB($this->dbName);
        $this->coll = $this->db->selectCollection('catalog');
    }

    /**
     * @param string $rubric
     * @return mixed|string
     */
    public function getRubricLink($rubric = '')
    {
        if (!$rubric){
            /**
             * @example https://yaca.yandex.ru/yca/ungrp/cat/
             * @example https://yaca.yandex.ru/yca/ungrp/cat/97.html
             */
            $link = self::BASE_URL . 'yaca/cat/';
        } else {
            /**
             * @example https://yaca.yandex.ru/yca/ungrp/cat/Computers/Computers/
             * @example https://yaca.yandex.ru/yca/ungrp/cat/Computers/Computers/1.html
             */
            $link = self::BASE_URL . ltrim($rubric, '/');

        }

        return $link;
    }

    /**
     * Set Class rubric
     * @param $data
     */
    public function setRubric($data)
    {
        $this->rubricName = $data['name'];
        $this->rubricLink = $data['link'];
    }

    /**
     * @return array
     */
    public function getRubrics()
    {
        $link = $this->getRubricLink();

        $dom = DOM::getDOM($link, mt_rand(0, 1));

        /* -----   */
//        $html = file_get_contents(ROOT_PATH . '/tests/app/modules/services/yandex/yandex_rubrics.html');
//        $dom  = DOM::makeDomFromHtml($html);
        /* -----   */

        $rubrics = [];

        /*
         * WARNING!!! Get only first level rubrics (TOP)
         * */
        $nodes = DOM::findByXPath('//div[@class="category"]/h3/a', $dom);

        /** @var \DOMElement $node */
        foreach($nodes as $i => $node){
            $link = $this->getRubricLink($node->getAttribute('href'));
            $rubrics[$node->nodeValue] = $link;
        }

        return $rubrics;
    }

    /**
     * Download Catalog by Top rubrics
     * @param int  $startPage
     * @param bool $pageLimit
     */
    public function downloadCatalog($startPage = 1, $pageLimit = false)
    {
        $rubrics = $this->getRubrics();
        foreach($rubrics as $name => $link){
            $this->setRubric([
                'name' => $name,
                'link' => $link,
            ]);
            $this->downloadCatalogByRubric($startPage, $pageLimit);
        }
    }

    /**
     * Scan and store elements from YandexCatalog
     * @param int    $startPage
     * @param bool   $pageLimit - max pages to scan. By default is unlimited
     *
     * @return bool
     */
    public function downloadCatalogByRubric($startPage = 1, $pageLimit = false)
    {
        if ($this->rubricLink){
            $nextPage = $this->rubricLink . ($startPage > 1 ? (--$startPage) . ".html" : '');
        } else {
            $nextPage = self::BASE_URL . 'yaca/cat/' . ($startPage > 1 ? (--$startPage) . ".html" : '');
        }

        do {
            $attempts = $this->maxAttempts;

            $pageDom  = $this->loadPage($nextPage);
            if (!$pageDom) {
                while($attempts && !$pageDom){
                    sleep(1);
                    $pageDom = $this->loadPage($nextPage);
                    $attempts--;
                }
                if (!$pageDom){
                    echo "\nFailed to load DOM for next page\n";
                    return false;
                }
            }

            $nextPage = $this->getNextPageLink($pageDom);

            echo "$nextPage\n";

            if (!$nextPage){
                echo "\nNo next pages found. Probably all items were downloaded\n";
                return true;
            }

            /*
             * Store or update items
             * */
            $items = $this->parsePage($pageDom);
            foreach($items as $item){
                $item['rubric'] = $this->rubricName;
                $item = CatalogItem::make($item);
                $this->saveItem($item);
            }

            if ($pageLimit !== false) {
                $pageLimit--;
                if (!$pageLimit) {
                    echo "\nStop loop by count\n";
                    return true;
                }
            }
        } while($nextPage);

        return true;
    }

    /**
     * @param string $pageUrl
     * @return bool|\DOMDocument , false if empty document passed
     */
    private function loadPage($pageUrl)
    {
        try {
            $dom = DOM::getDOM($pageUrl, mt_rand(0, 1));

            /** To dev */
//        $page = file_get_contents(ROOT_PATH . '/tests/app/modules/services/yandex/yandex_rubrics.html');
//        $page = file_get_contents(ROOT_PATH . '/tests/app/modules/services/yandex/yandex_rubrics_page_0.html');
//        $dom =  DOM::makeDomFromHtml($page);
            /** --- End To dev --- */

        } catch(\Exception $e){
            CDI()->devLog->log('Error on download YaCatalog :: ' . $e->getMessage());
            $dom = false;
        }

        return $dom;
    }

    /**
     * @param \DOMDocument $dom
     * @return string - empty if no next pages found
     */
    private function getNextPageLink(\DOMDocument $dom)
    {
        $query = "pager__pages";  // current page count
        $nodes = DOM::findByClass($query, $dom);
        $nextPage = '';

        /**
         * @var $node \DOMElement
         */
        foreach($nodes as $node){
            $current = false;
            /**
             * @var $c \DOMElement
             */
            foreach($node->childNodes as $c){

                if ($c instanceof \DOMElement){
                    if ($c->tagName == 'button'){
                        $current = true;
                        continue;
                    }

                    if ($c->tagName == 'a' && $current){
                        $nextPage = self::BASE_URL . ltrim($c->getAttribute('href'), '/');
                        break;
                    }
                }
            }
        }
        $nodes = null;

        if (!$nextPage)
            return false;

        return $nextPage;
    }

    /**
     * @param \DOMDocument $dom
     * @return array
     */
    private function parsePage(\DOMDocument $dom)
    {
        $query = "//ol/li[@class='yaca-snippet']";
        $nodes = DOM::findByXPath($query, $dom);

        $pageItems = [];

        /**
         * @var \DOMElement $node
         */
        foreach($nodes as $i => $node)
        {
            $tt = $dom->saveHTML($node);

            preg_match('/<a.*?class=\".*yaca-snippet__title.*?>(.+?)<\/a>/mi', $tt, $m);
            $title = array_pop($m);

            preg_match_all('/<span.*?>(.*?)<\/span>/mi', $tt, $m);
            $domain = strtolower(str_replace(['www.', 'http://', 'https://'], '', $m[1][0]));
            preg_match('/<div.*?class=\".*yaca-snippet__cy.*?>(.+?)<\/div>/mi', $tt, $m);
            $rating = (int)str_replace('ТИЦ:', '', array_pop($m));

            preg_match('/<a.*?class=\".*yaca-snippet__geo.*?>(.+?)<\/a>/mi', $tt, $m);
            $region = array_pop($m);

            $pageItems[] = [
                'title'  => $title,
                'domain' => $domain,
                'rating' => $rating,
                'region' => $region,
                'rubric' => ''
            ];
        }

        return $pageItems;
    }

    /**
     * Save item into DB. Or overwrite rating value if new one is bigger
     * @param CatalogItem $item
     */
    private function saveItem(CatalogItem $item)
    {
        try {
            $find = $this->coll->findOne([
                'domain' => $item->domain
            ]);

            if (!$find) {
                $ins = $this->coll->insert($item->toArray());
            } else {
                if ($find['rating'] < $item->rating) {
                    $upd = $this->coll->update(['_id' => $find['_id']], ['$set' => ['rating' => $item->rating]]);
                }
            }
        } catch(\Exception $e){
            CDI()->devLog->log("Error to store domain '".$item->domain ."' :: " . $e->getMessage());
        }
    }

    /**
     * Find an item by params
     * @example params = ['domain' => 'vk.com']
     *
     * @param array $params
     * @return array
     */
    public function findItem($params = [])
    {
        return $this->coll->findOne($params);
    }
}