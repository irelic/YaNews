<?php namespace yandex;
use Etechnika\IdnaConvert\IdnaConvert;
use \DOM;
use models\Sources;

/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.11.15
 * Time: 11:12
 */

class CategoryDomains
{

    private static $region_category_cache_key = 'reg_cat_%s_%s';

    /**
     * @param array $category
     * @param array $region
     */
    public static function process(array $category, array $region)
    {
        $time    = round(time()/60)*60;
        $domains = self::getCategorySources($category['url']);
        $new     = self::checkInCache($category['url'], $region['_id']) ? false : true;
        \Graphite::log('sources.sum', 1);
        foreach($domains as $domain) {
            $model            = new Sources();
            $model->domain    = IdnaConvert::decodeString($domain);
            $model->region_id = (int)$region['_id'];
            $model->url       = $category['url'];
            $model->new       = $new;
            $model->time      = $time;
            $model->save();
        }
    }

    /**
     * @param $link
     * @return array
     */
    public static function getCategorySources($link)
    {
        $link = $link.'&content=alldocs';
        $dom  = DOM::getDOM($link);

        $domains = self::getDomains($dom);

        $pages = Satellites::getPages($dom);

        for ($i = 1; $i < $pages; $i++) {
            $dom = DOM::getDom($link.'&content=alldocs&p='.$i, true);
            $domains = array_merge($domains, self::getDomains($dom));
        }

        return $domains;
    }


    private static function getDomains(\DOMDocument $dom)
    {
        $domains = [];

        $docs = DOM::findByXPath('//*[@class="doc"]//a', $dom);
        foreach($docs as $doc) {
            $href      = $doc->getAttribute('href');
            $domains[] = parse_url($href, PHP_URL_HOST);
        }

        return $domains;
    }

    /**
     * @param $region_id
     * @param $link
     * @return bool
     */
    private static function checkInCache($region_id, $link)
    {
        $key = md5(sprintf(self::$region_category_cache_key, $region_id, $link));

        if (CDI()->cache->setNx($key) == false) {
            return true;
        }

        CDI()->cache->expire($key, 2*24*60*60);

        return false;
    }

}