<?php
/**
 * Wrapper class for YandexCatalog Element
 *
 * @author :  ignat
 * @date   :  17.03.15 10:33
 */
namespace yandex;

class CatalogItem
{
    public $title;
    public $domain;
    public $rating;
    public $region;
    public $rubric;

    /**
     * Create and fill Yandex Catalog Item
     * @param array $params
     * @return CatalogItem
     */
    public static function make($params = [])
    {
        $item = new self();
        $item->title = isset($params['title']) ? str_replace(['"', "'"], '', html_entity_decode($params['title'])) : '';
        $item->domain = isset($params['domain']) ? $params['domain'] : '';
        $item->rating = isset($params['rating']) ? $params['rating'] : '';
        $item->region = isset($params['region']) ? $params['region'] : '';
        $item->rubric = isset($params['rubric']) ? $params['rubric'] : '';
        return $item;
    }

    /**
     * Convert CatalogItem Object to array
     * @return array
     */
    public function toArray()
    {
        return (array)$this;
    }
}