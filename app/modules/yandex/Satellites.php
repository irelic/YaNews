<?php
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.03.15
 * Time: 10:00
 */

namespace yandex;
use yandex\searcher;
use \DOM;

class Satellites
{

    /**
     * Get YandexNews Satellites posts on same thematic
     * @param $url
     * @return array
     */
    public static function processHosts($url)
    {
        $dom = DOM::getDom($url.'&content=alldocs', true);
        $hosts = [];
        $messageNodes = self::getNodes($dom);
        $hosts = array_merge($hosts, self::getHosts($messageNodes, $dom));
        $pages = self::getPages($dom);
        for ($i = 1; $i < $pages; $i++) {
            $dom = DOM::getDom($url.'&content=alldocs&p='.$i, true);
            $messageNodes = self::getNodes($dom);
            $hosts = array_merge($hosts, self::getHosts($messageNodes, $dom));
        }

        return $hosts;
    }

    /**
     * @param \DOMNodeList $nodes
     * @param \DOMDocument $dom
     * @return array
     */
    private static function getHosts(\DOMNodeList $nodes, \DOMDocument $dom)
    {
        $hosts = [];
        foreach ($nodes as $message) {
            $message_html = $dom->saveHTML($message);
            $hosts[] = self::getDomain($message_html);
        }

        return $hosts;
    }

    /**
     * @param $text
     * @return mixed|string
     */
    private static function getDomain($text)
    {
        $host = '';
        $source = preg_match('/<a[^>]+href="([^">]+)/', $text, $matches) ? strip_tags($matches[1]) : "";
        if ($source) {
            $host = parse_url($source, PHP_URL_HOST);
        }

        return $host;
    }

    /**
     * @param \DOMDocument $dom
     * @return \DOMNodeList
     */
    private static function getNodes(\DOMDocument $dom)
    {
        $messageNodes = DOM::findByXpath('//*[@class="story__group"]//*[@class="doc__title"]', $dom);

        return $messageNodes;
    }


    /**
     * @param \DOMDocument $dom
     * @return int
     */
    public static function getPages(\DOMDocument $dom)
    {
        $pages = DOM::findByXpath('//*[@class="pager__group"]/a[last()]/span', $dom);
        if ($pages->item(0)) {
            return (int)$pages->item(0)->nodeValue;
        } else {
            return 0;
        }
    }

}