<?php
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 16.03.15
 * Time: 17:46
 */
namespace yandex;

use \DOM;
use models\Items;
use models\Regions;

class Top
{
    private $domain = 'http://news.yandex.ru';

    /**
     * @param $region_id string
     * @return \models\Items[]
     */
    public function getTop($region_id = null)
    {
        if ($region_id) {
            $region = Regions::findFirst([['string_id' => $region_id]]);
        }
        
        $time = round(time()/60)*60;
        $domain = $region_id ? $this->domain . '/'. $region_id : $this->domain;
        $domain = $domain . '?lang=ru';
        $dom = DOM::getDOM($domain, true);
        $links = DOM::findByXpath('//*[contains(@class , "rubric_type_main")]//*[@class="story__title"]/a', $dom);
        $array = [];
        $i = 1;
        foreach ($links as $link) {
            $container = new Items();
            $container->setPosition($i);
            $container->setLink($this->cutRegionFromLink($this->domain.$link->getAttribute('href')));
            $container->setText($link->textContent);
            $container->setTime($time);
            if (isset($region) && !empty($region)) {
                $container->setRegion($region->_id);
            }
            $array[] = $container;
            $i++;
        }

        return $array;
    }

    public function getRegionTop($string_id)
    {
        $this->domain = 'https://news.yandex.ru';
        $dom = DOM::getDOM($this->domain.'/'.$string_id.'/index.html');
        $nodes = DOM::findByXPath('//*[contains(@class , "story_pos_first")]//*[@class="story__title"]/a|//*[@class="story"]//*[@class="story__title"]/a', $dom);
        $i = 0;
        $top = [];
        foreach($nodes as $node) {
            $top[$i]['url']  = $this->cutRegionFromLink($this->domain.$node->getAttribute('href'));
            $top[$i]['text'] = $node->textContent;
            $i++;
        }

        return $top;
    }

    private function cutRegionFromLink($url)
    {
        $url   = parse_url($url);
        parse_str($url['query'], $parse_query);
        $query['cl4url'] = $parse_query['cl4url'];

        return $url['scheme'] .'://'. $url['host'] . $url['path'] .'?'. http_build_query($query);
    }

}