<?php
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.03.15
 * Time: 11:07
 */
namespace yandex;
use App\Exceptions\ContentEmptyException;
use \DOM;
use App\Exceptions\YandexCaptchaException;
use models\Items;
use models\Tcy;
use queue\QueueException;

class Ratings
{

    private static $url_template = 'https://yandex.ru/yaca/cy/ch/';
    private static $table_class = 'cy__table';
    private static $error_class = 'cy__not-described-cy';
    public static $rating_cache_key = 'rating_%s';

    /**
     * @param array $msgContainer
     * @return Items
     * @throws QueueException
     */
    public static function processRating(array $msgContainer)
    {
        $time = microtime(true);
        $hosts = Satellites::processHosts($msgContainer['link']);

        \Graphite::log('hosts_time.avg', microtime(true) - $time);

        $count = count($hosts);

        $rating_sum = 0;
        $time = microtime(true);
        foreach ($hosts as $host) {
            $rating = self::getRating($host);
            $rating_sum += $rating;
        }

        \Graphite::log('ratings_time.avg', microtime(true) - $time);

        $obj = new Items();
        $obj->makeFromArray($msgContainer);

        if ($rating_sum == 0 || $count == 0) {
            \Graphite::log('resend.sum', 1);
            throw new QueueException("Release job with 1 min delay");
        }

        $obj->setRating($rating_sum);
        $obj->setHosts($count);

        return $obj;
    }

    /**
     * @param $domain
     * @param bool $needException
     * @return int
     * @throws \Exception
     */
    public static function getRating($domain, $needException = false)
    {
        $domain = Tcy::stripDomain($domain);
        $key    = sprintf(self::$rating_cache_key, $domain);
        $rating = CDI()->cache->getKey($key);

        if ($rating !== false) {
            return (int)$rating;
        }

        try {
            $model = Tcy::findByDomain($domain);

            if ($model === false) {
                $dom    = DOM::getDOM(self::$url_template . $domain);
                $rating = self::findRating($dom);

                Tcy::saveTcy($domain, $rating);
                self::setCache($key, $rating);

                return (int)$rating;
            }

            if (!$model->isOld()) {
                $rating = $model->tcy;
            } else {
                $dom    = DOM::getDOM(self::$url_template.$domain);
                $rating = self::findRating($dom);

                $model->tcy = $rating;
                $model->save();
            }

            self::setCache($key, $rating);

            return (int)$rating;
        } catch (\Exception $e) {
            if ($needException) {
                throw $e;
            }

            return 0;
        }
    }

    /**
     * @param \DOMDocument $dom
     * @return int
     * @throws ContentEmptyException
     * @throws YandexCaptchaException
     */
    private static function findRating(\DOMDocument $dom)
    {
        if (YandexError::isContentEmpty($dom)) {
            throw new ContentEmptyException();
        }
        
        if (YandexError::isCaptcha($dom)) {
            throw new YandexCaptchaException();    
        }

        $trs = DOM::findByXpath('//*[@class="'.self::$table_class.'"]//tr', $dom);
        $rating = 0;
        if ($trs->length > 0) {
            foreach ($trs as $tr) {
                $html = $dom->saveHTML($tr);
                if (self::findHit($html) && $tr->getElementsByTagName('td')->length > 0) {
                    $rating = (int)preg_replace('/[^0-9]+/', '', $tr->getElementsByTagName('td')->item(2)->nodeValue);
                    break;
                }
            }
        } else {
            $tr = DOM::findByXpath('//*[@class="'.self::$error_class.'"]', $dom);
            if ($tr->length > 0) {
                $value = $tr->item(0)->nodeValue;
                $rating = (int)preg_replace('/[^0-9]+/', '', $value);
            }
        }

        return $rating;
    }

    /**
     * @param $text
     * @return int
     */
    private static function findHit($text)
    {
        return preg_match('/cy__description/sU', $text);
    }

    private static function setCache($key, $rating)
    {
        CDI()->cache->setKey($key, $rating);
        CDI()->cache->expire($key, 7*24*60*60);
    }


}