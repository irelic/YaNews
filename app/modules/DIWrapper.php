<?php
/**
 * DI wrapper
 *
 * @author :  ignat
 * @date   :  24.06.14 15:33
 */

/**
 * Class wrapper for Phalcon\DI
 * Used to organize and auto complete for IDE main DI component.
 *
 * @pattern singleton
 *
 * @property Phalcon\Mvc\Url $url
 * @property Phalcon\Mvc\Router\Annotations $router
 * @property Phalcon\Mvc\Dispatcher $dispatcher
 * @property Phalcon\Mvc\View $view
 *
 * @property Phalcon\Mvc\Model\Metadata\Memory $modelsMetadata
 *
 * @property \Phalcon\Events\ManagerInterface $eventsManager
 *
 * @property MongoClient $mongoConnection
 * @property MongoDb $mongo
 * @property Phalcon\Config $config
 *
 * @property Phalcon\Mvc\Collection\Manager $collectionManager
 * @property Logger $devLog
 * @property Logger $gvEntryLog
 * @property Logger $toneLog
 * @property Logger $profileLog
 * @property Logger $queryLog
 * @property Logger $analyzerLog
 * @property Logger $elasticLog
 *
 *
 * @property storage\RedisCache $redis
 *
 * @property \helper\Cache $cache
 *
 * @property \ClientResolver $clientResolver
 */
class DIWrapper {

    private static $instance;
    /**
     * @var \Phalcon\DI\FactoryDefault
     */
    private $container;

    private function __construct($di)
    {
        $this->container = $di;
    }

    /**
     * Get instance of container
     * @param $di - global DI object
     * @return DIWrapper
     */
    public static function getInstance($di){

        if ( !self::$instance )
            self::$instance = new self($di);

        return self::$instance;
    }

    public function __get($name)
    {
        $default = $this->container->getDefault();
        return ($default->has($name) ? $default->get($name) : null);
    }
}