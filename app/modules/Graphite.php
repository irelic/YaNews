<?php
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 16.03.16
 * Time: 14:58
 */

class Graphite
{

    public static function log($metrics, $value)
    {
        $hostname = trim(gethostname());
        if ($hostname == 'yanews-production') {
            CDI()->graphite->measure("$hostname.yanews.$metrics", $value);
            CDI()->graphite->flush();
        }
    }

}