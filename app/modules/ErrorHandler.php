<?php
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.01.15
 * Time: 10:36
 */

class ErrorHandler
{
    private static $fatal_levels = [E_ERROR, E_PARSE, E_COMPILE_ERROR, E_CORE_ERROR];
    private static $application_levels = ['web', 'cli'];
    private static $level;

    private function __construct($level = 'web')
    {
        if (in_array($level, self::$application_levels)) {
            self::$level = $level;
        } else {
            self::$level = 'web';
        }
    }

    public static function register($version = 'web')
    {
        $obj = new self($version);
        set_error_handler([$obj, 'catchError']);
        register_shutdown_function([$obj, 'catchFatal']);
    }

    public function catchError($errno, $errstr, $file, $line)
    {
        switch ($errno) {
            case E_WARNING:
                $level = 'WARNING';
                break;
            case E_NOTICE:
                $level = 'NOTICE';
                break;
            default:
                $level = $errno;
                break;
        }
        self::logError('['.self::$level.'] Error level: '. $level . '. Message: '. $errstr . '. File: "'. $file . '" on line '. $line);
        return false;
    }

    public function catchFatal()
    {
        $error = error_get_last();
        if (isset($error) && in_array($error['type'], self::$fatal_levels)) {
            $message = '['.self::$level.'] Fatal error level: ' . $error['type'] . '. Message: "' . $error['message'] .
                '" in file ' . $error['file'] . ' on line ' . $error['line'];
            self::logError($message);
            if (self::$level === 'web')
                self::showErrorPage();
        }
    }

    public static function logError($message)
    {
        if (!class_exists('Logger')) {
            require ROOT_PATH . '/app/modules/Logger.php';
        }
        $logger = new \Logger(ROOT_PATH . '/logs/err_handler.log', true);
        $logger->log($message);
    }

    public static function showErrorPage()
    {
        require(ROOT_PATH . '/app/views/error.html');
    }

    public static function exception(\Exception $e)
    {
        self::logError('['.self::$level.'] Exception: '. $e->getMessage(). "\n". $e->getTraceAsString());
        if (self::$level === 'web')
            self::showErrorPage();
    }


}