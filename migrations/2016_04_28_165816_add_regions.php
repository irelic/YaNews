<?php

use migrator\IMigration;
use yandex\YandexRegions;

class AddRegionsMigration implements IMigration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $regions = (new YandexRegions())->getRegions();
        
        foreach($regions as $region) {
            $model = new \models\Regions();
            $model->_id = (int)$region['id'];
            $model->string_id = $region['string_id'];
            $model->text = $region['text'];
            $model->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $regions = \models\Regions::find();

        foreach($regions as $region) {
            $region->delete();
        }
    }

}
