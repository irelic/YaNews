<?php

use migrator\IMigration;
use models\Tcy;

class FillTcyFromCacheMigration implements IMigration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $keys = CDI()->redis->getInstance()->keys('yanews_rating_*');

        foreach($keys as $key) {
            $key = str_replace('yanews_', '', $key);
            $domain = str_replace('rating_', '', $key);
            $domain = Tcy::stripDomain($domain);

            $rating = (int)CDI()->cache->getKey($key);

            if ($rating > 0) {
                Tcy::saveTcy($domain, $rating);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }

}
