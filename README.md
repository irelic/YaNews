# YaNews
Система анализа новостей Яндекса
### Зависимости
- [PHP](http://php.net) >= 5.6

- [Redis](http://redis.io/) >= 2.8.4

- [Beanstalkd](http://kr.github.io/beanstalkd/) >= 1.10

- [MongoDb](https://www.mongodb.org/downloads) > 3.0

- [Phalcon](https://www.phalconphp.com/) > 2.0.6

### Подготовка
```
$ sudo apt-get install nodejs
```
### Запуск проекта
```
$ sudo ant
```