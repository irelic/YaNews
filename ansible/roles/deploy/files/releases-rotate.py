#!/usr/bin/python
import sys
import os
import shutil


def run(path):
    dirs= sorted([ f for f in os.listdir(path)])
    if len(dirs) > 5:
        for i in dirs[0:len(dirs)-5]:
            shutil.rmtree(path + '/' + i)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        run(sys.argv[1])
