<?php
error_reporting(E_ALL);
set_time_limit(0);
use Phalcon\CLI\Console as ConsoleApp;
use Phalcon\DI\FactoryDefault\CLI as CliDI;

try {

    if (!defined('ROOT_PATH')) {
        define('ROOT_PATH', dirname(__DIR__));
    }

    require( ROOT_PATH . '/app/modules/ErrorHandler.php');
    ErrorHandler::register('cli');

    $di = new CliDI();

    $config = include ROOT_PATH . "/app/config/config.php";
    require_once ROOT_PATH . "/vendor/autoload.php";
    include_once ROOT_PATH . "/app/config/loader.php";
    include_once ROOT_PATH . "/app/config/services.php";

    $console = new ConsoleApp();
    $console->setDI($di);

    $params = [];
    $action = 'main';
    $task   = '';
    $module = array_shift($argv);

    CDI()->clientResolver->setClient('yanews');

    if (count($argv)) {
        $task = array_shift($argv);
        if (count($argv)) {
            $action = array_shift($argv);
            $params = $argv;
        }
    }

    $arguments = array_merge(['task' => $task, 'action' => $action], $params);

    $console->handle($arguments);

} catch (\Exception $e) {
    ErrorHandler::exception($e);
}
