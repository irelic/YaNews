#!/bin/bash

# ##############################################
# Start local PHP server
# ##############################################

DIR=`dirname $0`
ROOT="${DIR}/../.."

echo "Start local web server"
/usr/bin/php -S 0.0.0.0:8080 -t "${ROOT}/public" "${ROOT}/public/.htrouter.php" >"${ROOT}/logs/webserver.out" 2>"${ROOT}/logs/webserver.err"&
echo "[ OK ]"

