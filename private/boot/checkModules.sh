#!/bin/bash

# ###############################################
#
# Check dependencies and installed modules
#
# ###############################################

OS=`uname`

REDIS_SRV_VERSION='2.4'
REDIS_CLI_VERSION='2.4'

MONGO_VERSION='2.6.1'
BEANSTALK_VERSION='1.9'
ELASTIC_VERSION='1.2.1'

# ###############################################
# ###############################################

function on_error
{
    echo -e "\n$1 \n[ EXIT ]"; exit 1
}

function checkOS
{
    if [[ "$OS" != "Linux" && "$OS" != "Darwin" ]]
    then
        on_error "Unsupported OS type. Exit.";
    fi
}

function checkPHP
{
    if [ -z `which php` ]
    then
        on_error "Php is not installed";
    fi

    echo "[ check ] PHP version :: "`php -v | head -n1 | awk '{print $2}'`
    echo "[ check ] modules"

    local MODULES=`php -r "print_r(get_loaded_extensions());"`
    local EXTENSIONS=(phalcon mongo redis)

    for i in ${!EXTENSIONS[*]}
    do
        if [[ `echo ${MODULES} | grep ${EXTENSIONS[$i]}` == "" ]]
        then
            on_error "${EXTENSIONS[$i]} does not installed as php module"
        fi
    done
}

# ###############################################
# Compare verions of modules
# Params: $1 - version string 1 (ex. "1.10")
# Params: $2 - version string 2 (ex. "1.4.7-10")
# Return: Bool, if v1 > v2
# ###############################################
function compare_version
{
    local a=`echo $1 | sed 's/[-|\.]/ /g'`
    local b=`echo $2 | sed 's/[-|\.]/ /g'`

    local lenA=`echo ${a} | wc -w`
    local lenB=`echo ${b} | wc -w`

    a=$(echo ${a} | tr -d ' ')
    b=$(echo ${b} | tr -d ' ')

    if [[ "$lenA" > "$lenB" ]]
    then
        diff=$(echo "$lenA - $lenB" | bc)
        b=$(echo "$b * (10^$diff)" | bc)
    fi

    if [[ "$lenB" > "$lenA" ]]
    then
        diff=$(echo "$lenB - $lenA" | bc)
        a=$(echo "$a * (10^$diff)" | bc)
    fi

    if [[ "$a" -lt "$b" ]]
    then
        echo 1
    else
        echo 0
    fi
}

function checkDrivers
{
    local TOOLS=('beanstalkd' 'mongo' 'redis-server' 'redis-cli')

    echo "[ check ] drivers"

    for i in ${!TOOLS[*]}
    do
        echo " -> test :: ${TOOLS[$i]}"
        if [ -z `which ${TOOLS[$i]}` ]
        then
            on_error "${TOOLS[$i]} is not installed";
        fi
    done

    local V_BS=`beanstalkd -v | awk '{print $2}' | cut -d '+' -f 1`
    if [ $(compare_version "${V_BS}" "${BEANSTALK_VERSION}") -ne 0 ]
    then
        on_error "Beanstalk version ${V_BS} is lower than ${BEANSTALK_VERSION}"
    fi

    local V_MG=`mongo --version | cut -d ':' -f2 | tr -d ' '`
    if [ $(compare_version "${V_MG}" "${MONGO_VERSION}") -ne 0 ]
    then
        on_error "MongoDB version ${V_MG} is lower than ${MONGO_VERSION}"
    fi

    local V_RSRV=`redis-server -v | awk '{print $3}' | tr -d 'v='`
    if [ $(compare_version "${V_RSRV}" "${REDIS_SRV_VERSION}") -ne 0 ]
    then
        on_error "Redis server version ${V_RSRV} is lower than ${REDIS_SRV_VERSION}"
    fi

    local V_RCLI=`redis-cli -v | awk '{print $2}'`
    if [ $(compare_version "${V_RCLI}" "${REDIS_CLI_VERSION}") -ne 0 ]
    then
        on_error "Redis client version ${V_RCLI} is lower than ${REDIS_CLI_VERSION}"
    fi
}

# ###############################################
# ###############################################

checkOS
checkPHP
checkDrivers

echo "Modules are up-to-date"