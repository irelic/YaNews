#!/bin/bash

# ##############################################
# Configure for stage building
# ##############################################

function on_error
{
    echo -e "$1 \n[ EXIT ]"; exit 1
}

# ##############################################
# Modules functions
# ##############################################

function load_modules
{
    if [ `ps ax | grep beanstalkd | grep -v grep | wc -l` -lt "0" ]
    then
        echo "Start beanstalk service queue"
        nohup beanstalkd &
    fi

    if [ `ps ax | grep redis-server | grep -v grep | wc -l` -gt "0" ]
    then
        echo "Redis server is already running"
    else
        echo "Start Redis server"
        nohup redis-server &
        sleep 1

        if [ `ps ax | grep redis-server | grep -v grep | wc -l` -eq "0" ]
        then
            on_error "An error occurred during load Redis server"
        fi
    fi

    # if [ `ps ax | grep elasticsearch | grep -v grep | wc -l` -eq "0" ]
    # then
    #     on_error "Elasticsearch is not running. Please make shure to start it!"
    # fi
}

# ##############################################
# Start functions
# ##############################################

function get_status
{
    local errors=0

    echo 'Processes status'

    for K in "${!WORKERS[@]}"; do
        local ACTIVE=`get_process_status $K`
        echo -n "$K :: ${ACTIVE} / ${WORKERS[$K]} "
        if test $ACTIVE -ge ${WORKERS[$K]}
        then echo ' [OK]'
        else echo ' [FAIL]'; ((errors++))
        fi
    done

    echo ""

    local MONGO=`ps ax | grep "mongod" | grep -v grep | wc -l`
    # local ELASTIC=`ps ax | grep "elasticsearch" | grep -v grep | wc -l`
    local BEANSTALK=`ps ax | grep "beanstalkd" | grep -v grep | wc -l`
    local REDIS=`ps ax | grep "redis-server" | grep -v grep | wc -l`

    echo -n "Mongo         :: "
    if `test ${MONGO} -ge 1`
    then echo ' [OK]'
    else echo ' [FAIL]'; ((errors++))
    fi
    # echo -n "Elasticsearch :: "
    # if `test ${ELASTIC} -ge 1`
    # then echo ' [OK]'
    # else echo ' [FAIL]'; ((errors++))
    # fi
    echo -n "Beanstalk     :: "
    if `test ${BEANSTALK} -ge 1`
    then echo ' [OK]'
    else echo ' [FAIL]'; ((errors++))
    fi
    echo -n "Redis         :: "
    if `test ${REDIS} -ge 1`
    then echo ' [OK]'
    else echo ' [FAIL]'; ((errors++))
    fi

    if `test $errors -gt 0`
    then
        on_error "Workers are failed!!! (${errors})"
    fi
}

# ###########################
# Param: $1 - process name (e.g. filter, grabber, etc.)
# ###########################
function get_process_status
{
    NAME="${@:1}"

    local P=`ps ax | grep "${CLI_TAG}" | grep -v grep | grep "${NAME}" | wc -l | tr -d ' '`

    echo "$P"
}

# ###########################
# Start build-in PHP server
# in development mode
# ###########################
function start_server
{
    local STAGE=`cat ${ENV} | grep "APP_ENV" | sed  "s/.*=//g" | awk '{print tolower($1)}'`
    if [[ "${STAGE}" == "development" ]]
    then
        ${DIR}/localboot.sh
    fi
}

function stop_server
{
    local SRV=`ps ax | grep "${ROOT}/public" | grep -v grep`
    if [[ "${SRV}" != "" ]]
    then
        echo "Stop local PHP server"
        echo "${SRV}" | awk '{print $1}' | xargs kill -9
    fi
}

function restart_server
{
    stop_server && start_server
}

# ###########################
# Load worker by passed name
# Params: $1 - worker name
#         $2 - instance count
#         $3 - params
# ###########################
function load_worker
{
    local max=$1
    local name=$2

    ARGS="${@:2}"

    if [ -z ${name} ]
    then
        echo "No worker name passed!"
        return 0
    fi

    if [ -z ${max} ]
    then
        max=1
    fi

    echo "Start workers for [ ${ARGS} ]"

    for (( c = 1; c <= $max; c++ ))
    do
        nohup "php" "${CLI}" "${@:2}" 2>&1 > "${ROOT}/logs/${name}.log" &
    done

    sleep 1

    if [ `ps ax | grep "${CLI}" | grep -v grep | wc -l` -lt "${max}" ]
    then
        on_error "An error occurred during start [ ${ARGS} ] process"
    fi
}

function start
{
    load_modules &&

    for K in "${!WORKERS[@]}"; do
        load_worker ${WORKERS[$K]} $K
    done

    log "Start system "
}

# ##############################################
# ##############################################
# Stop functions
# ##############################################

function stop
{
    echo "Stop running processes"
    for pid in `ps ax | grep "${CLI_TAG}" | grep -v grep | awk '{print $1}'`
    do
        kill -9 ${pid}
    done

    log "Stop system processes"
}

function log
{
    echo "["`date`"] ${1}" >> "${LOG}"
}

# ##############################################
# Start executing
# ##############################################

OPTION=`echo $1 | tr '[:upper:]' '[:lower:]'`

DIR=`dirname $0`
ROOT="$(dirname $(dirname "$DIR"))"

. "${DIR}/config.cfg"

CLI="${ROOT}/private/cli.php"
CLI_TAG="/private/cli.php"

LOG="${ROOT}/logs/system.log"

case "$OPTION" in

    'start'  ) start ;;
    'stop'   ) stop  ;;
    'status' ) get_status  ;;
    'restart') stop && start && echo "Restart complete" ;;

    'start_server'   ) start_server ;;
    'stop_server'    ) stop_server ;;
    'restart_server' ) restart_server ;;

    'load_modules'   ) load_modules ;;

    *)
        echo "Using: $0 <OPTION>"
        echo -e "\tOPTION - [ start|stop|restart|status|start_server|permissions ]"
        exit 1
    ;;
esac
