#!/bin/bash

# ###############################################
#
# Prepare local environment for launch viewer_web
#
# ##############################################

DIR=`dirname $0`
${DIR}/configure.sh "start"

exit