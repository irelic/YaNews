<?php
use yandex\YandexError;

/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 16.05.16
 * Time: 10:50
 */

class YandexErrorTest extends PHPUnit_Framework_TestCase
{

    public function testIsContentEmpty()
    {
        $dom = new DOMDocument('1.0', 'UTF-8');

        $this->assertTrue(YandexError::isContentEmpty($dom));
    }

    public function testIsCaptcha()
    {
        $captcha = file_get_contents('captcha.html');
        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom->loadHTML($captcha);

        $this->assertEquals(1, YandexError::isCaptcha($dom));
    }

}