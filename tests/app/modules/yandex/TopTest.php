<?php
use yandex\YandexRegions;

/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.11.15
 * Time: 12:07
 */

class TopTest extends PHPUnit_Framework_TestCase
{

    public function testGetTop()
    {
        $regions = (new YandexRegions())->getRegions();

        foreach($regions as $region) {
            $model = new \models\Regions();
            $model->_id = (int)$region['id'];
            $model->string_id = $region['string_id'];
            $model->text = $region['text'];
            $model->save();
        }

        $top = (new \yandex\Top())->getTop();
        $this->assertCount(5, $top);
        foreach($top as $item) {
            $this->assertInstanceOf('models\\Items', $item);
            $this->assertObjectHasAttribute('time', $item);
            $this->assertObjectHasAttribute('position', $item);
            $this->assertObjectHasAttribute('link', $item);
            $this->assertObjectHasAttribute('text', $item);
        }
    }

    public function testGetRegionTop()
    {
        $region = (new \yandex\YandexRegions())->getRegions()[0];
        $regions_top = (new \yandex\Top())->getRegionTop($region['string_id']);
        $this->assertNotEmpty($regions_top);
        $this->assertCount(5, $regions_top);
        foreach($regions_top as $item) {
            $this->assertArrayHasKey('text', $item);
            $this->assertArrayHasKey('url', $item);
        }
    }

}