<?php
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.03.15
 * Time: 10:22
 */

use yandex\Ratings;

class RatingsTest extends PHPUnit_Framework_TestCase
{
    public function testGetRating()
    {
        CDI()->clientResolver->setClient('test');

        $link = 'www.nabludatel.ru';
        $this->assertEquals(1200, Ratings::getRating($link));
        $link = 'sledcomrf.ru';
        $this->assertEquals(1100, Ratings::getRating($link));
        $link = 'rg.ru';
        $this->assertEquals(35000, Ratings::getRating($link));
        $link = 'izvestia.ru';
        $this->assertEquals(21000, Ratings::getRating($link));
        $link = 'test123213123.ru';
        $this->assertEquals(0, Ratings::getRating($link));
        //rating was 0
        $link = 'pomorie.ru';
        $this->assertEquals(0, Ratings::getRating($link));
    }

    public function testProcessRating()
    {
        $msgContainer = [
            'time' => 1426593557,
            'link' => 'http://news.yandex.ru/yandsearch?cl4url=www.gazeta.ru/politics/news/2015/03/17/n_7020573.shtml&amp;lang=ru&amp;lr=213',
            'text' => 'Песков посоветовал Псаки не вмешиваться во внутренние дела России',
            'position' => 2,
            'rating' => 0
        ];
        $object = Ratings::processRating($msgContainer);

        $this->assertGreaterThan(0, $object->getRating());
    }

}