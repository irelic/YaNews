<?php
use models\Sources;
use yandex\CategoryDomains;

/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 16.03.16
 * Time: 11:36
 */

class CategoryDomainsTest extends PHPUnit_Framework_TestCase
{

    public function testProcess()
    {
        CDI()->mongo->selectCollection(Sources::$_collection)->remove();

        $category = [
            'url' => 'https://news.yandex.ru/yandsearch?cl4url=1prime.ru%2FNews%2F20160316%2F824224275.html&lr=47&lang=ru',
            'text' => 'США связали отмену санкций против России с возвращением Крыма Украине'
        ];
        $region = [
            'string_id' => 'Barnaul',
            'text' => 'Барнаульская область',
            '_id' => '197'
        ];

        CategoryDomains::process($category, $region);

        $this->assertNotEmpty(Sources::find());
    }


    public function testGetCategorySources()
    {
        $link = 'https://news.yandex.ru/yandsearch?cl4url=www.ntv.ru%2Fnovosti%2F1627743%2F&lr=47&lang=ru';
        $this->assertNotEmpty(CategoryDomains::getCategorySources($link));
    }
}