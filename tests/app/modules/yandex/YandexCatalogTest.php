<?php
/**
 * Short product info
 *
 * @author :  ignat
 * @date   :  16.03.15 18:19
 */


use yandex\YandexCatalog;


class YandexCatalogTest extends PHPUnit_Framework_TestCase
{

    public function testDownloadCatalog()
    {
        $this->markTestSkipped();
        $ya = new YandexCatalog();

        $ya->downloadCatalogByRubric();
        $ya->downloadCatalogByRubric(98, 10);

        $ya->downloadCatalog();

    }

    public function testFindItem()
    {
        $ya = new YandexCatalog();
        $found = $ya->findItem(['domain' => 'vk.com']);
        $this->assertNotEmpty($found);
    }

    public function testGetRubrics()
    {
        $ya = new YandexCatalog();
        $rubrics = $ya->getRubrics();
        $this->assertNotEmpty($rubrics);
    }
}
