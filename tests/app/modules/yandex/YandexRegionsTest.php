<?php

use \yandex\YandexRegions;

/**
 * YandexRegions test
 * @author :  nikolaev
 * @date   :  20.01.2015
 * @coversDefaultClass YandexRegions
 */
class YandexRegionsTest extends PHPUnit_Framework_TestCase
{

    /**
     * @covers ::getRegions
     */
    public function testGetRegions()
    {
        $class = new YandexRegions();
        $res = $class->getRegions();
        $this->assertInternalType('array', $res);
        $this->assertNotEmpty($res);
    }

    /**
     * @covers ::force
     */
    public function testForce()
    {
        $this->markTestSkipped();
        $class = new YandexRegions();
        $res = $class->force();
        $this->assertInternalType('array', $res);
        $this->assertNotEmpty($class->force());
    }

    /**
     * @covers ::getFromFile
     */
    public function testGetFromFile()
    {
        $class = new YandexRegions();
        $this->assertNotEmpty($class->getFromFile());
    }


}
 