<?php
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.05.16
 * Time: 11:10
 */

class TcyTest extends PHPUnit_Framework_TestCase
{

    public function testFindByDomain()
    {
        $collection = CDI()->mongo->selectCollection(\models\Tcy::$_collection);
        $collection->drop();

        $model = new \models\Tcy();
        $model->domain = 'vk.com';
        $model->tcy = 36000;
        $model->last_update = time();
        $this->assertTrue($model->save());
        
        $tcy = \models\Tcy::findByDomain('http://vk.com');
        $this->assertInstanceOf(\models\Tcy::class, $tcy);

        $tcy = \models\Tcy::findByDomain('http://test.com');
        $this->assertFalse($tcy);
    }

    public function testSaveTcy()
    {
        $collection = CDI()->mongo->selectCollection(\models\Tcy::$_collection);
        $collection->drop();

        $model = new \models\Tcy();
        $model->domain = 'vk.com';
        $model->tcy = 37000;
        $model->last_update = time();
        $this->assertTrue(\models\Tcy::saveTcy($model));
        $this->assertFalse(\models\Tcy::saveTcy($model));
    }

}