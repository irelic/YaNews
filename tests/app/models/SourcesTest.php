<?php
use models\Sources;

/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 16.03.16
 * Time: 12:07
 */

class SourcesTest extends PHPUnit_Framework_TestCase
{

    public function testGetSources()
    {
        CDI()->mongo->selectCollection(Sources::$_collection)->remove();

        $sources = '[{"region_id":959,"url":"https://news.yandex.ru/yandsearch?cl4url=rusplt.ru%2Fregion-news%2Fsevastopol%2Ftri-cheloveka-v-sevastopole-umerli-ot-pereohlajdeniya-505311%2F&lr=213&lang=ru","domain":"novocrimea.ru","time":1452541740,"new":false},'.
            '{"region_id":959,"url":"https://news.yandex.ru/yandsearch?cl4url=rusplt.ru%2Fregion-news%2Fsevastopol%2Ftri-cheloveka-v-sevastopole-umerli-ot-pereohlajdeniya-505311%2F&lr=213&lang=ru","domain":"test.ru","time":1452541740,"new":false}]';

        $sources = json_decode($sources, true);
        foreach ($sources as $source) {
            CDI()->mongo->selectCollection(Sources::$_collection)->insert($source);
        }

        $res = (new Sources())->getSources(959);
        $this->assertCount(2, $res);
        foreach($res as $item){
            $this->assertArrayHasKey('total', $item);
            $this->assertEquals(1, $item['categories_count']);
        }
    }

}