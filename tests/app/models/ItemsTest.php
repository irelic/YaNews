<?php
use models\Items;

/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 16.03.16
 * Time: 12:07
 */

class ItemsTest extends PHPUnit_Framework_TestCase
{

    public function testGetItems()
    {
        CDI()->mongo->selectCollection(Items::$_collection)->remove();

        $items = '[{"time":1426697640,"link":"http://news.yandex.ru/yandsearch?cl4url=1prime.ru/News/20150318/805159232.html&amp;lang=ru&amp;lr=213","text":"Россия и Южная Осетия подписали договор о союзе","position":4,"rating":1737155,"hosts":191},'.
            '{"time":1426697640,"link":"http://news.yandex.ru/yandsearch?cl4url=www.aif.ru/incidents/1470052&amp;lang=ru&amp;lr=213","text":"Более 90 полицейских ранены, 16 демонстрантов арестованы во Франкфурте","position":5,"rating":2412865,"hosts":357},'.
            '{"time":1426697640,"link":"http://news.yandex.ru/yandsearch?cl4url=rusnovosti.ru/posts/367529&amp;lang=ru&amp;lr=213","text":"Уход Opel с российского рынка не скажется на гарантийном обслуживании","position":1,"rating":1192840,"hosts":245},'.
            '{"time":1426697640,"link":"http://news.yandex.ru/yandsearch?cl4url=www.gazeta.ru/sport/news/2015/03/18/n_7024521.shtml&amp;lang=ru&amp;lr=213","text":"Дублирующий состав «Арсенала» проведет два матча за два дня","position":2,"rating":2182795,"hosts":462},'.
            '{"time":1426697640,"link":"http://news.yandex.ru/yandsearch?cl4url=www.ng.ru/news/497185.html&amp;lang=ru&amp;lr=213","text":"В Тунисе от рук боевиков погибли 17 туристов","position":3,"rating":3440035,"hosts":447}]';

        $items = json_decode($items, true);
        foreach ($items as $item) {
            CDI()->mongo->selectCollection(Items::$_collection)->insert($item);
        }

        $res = (new Items())->getItems(1426697580, 1426697700);

        $this->assertCount(1, $res);
        $this->assertCount(5, $res[0]['items']);
    }

}