<?php
use queue\QueueFactory;
use yandex\YandexRegions;

/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.11.15
 * Time: 16:39
 */

class SourcesTaskTest extends PHPUnit_Framework_TestCase
{

    public function testMainAction()
    {
        $this->markTestSkipped('for debug usage only');
        $regions = (new YandexRegions())->getRegions();
        QueueFactory::getQueue('process_regions_test')->send($regions[0]);
        QueueFactory::getQueue('process_regions_test')->processJobs([$this, 'process'], false);
    }

    public function process($job)
    {
        (new SourcesTask())->processRegion($job);
    }


}