<?php
/**
 * Created by PhpStorm.
 * User: nikolaev
 * Date: 17.03.15
 * Time: 19:33
 */
loadDepends('/app/tasks/MainTask.php');
loadDepends('/app/tasks/GrabberTask.php');


class GrabberTaskTest extends PHPUnit_Framework_TestCase
{
    public function test()
    {
        $this->markTestSkipped('for debug usage only');
        $task = new GrabberTask();
        $task->mainAction();
    }
}